#!/usr/bin/env python

from toolbox import houseKeeper
from toolbox import fileDownloader as fd
from toolbox.baseParser import BaseParser
from toolbox.mappingFilesParser import UniProtfileParser
from toolbox.mappingFilesParser import NCBIfileParser
import re, os
import subprocess
import pandas as pd

"""
   ------ RETRIEVE AND PREPARE MAPPING FILES ---------

All default options, column number where the taxon is placed,
can be changed in mappingFileParser.py script. A taxon list
can be passed with class instantiations.
"""


def main():

    ## 1. Create and/or pick location handlers of input/output folders and paths:
    hk = houseKeeper.HouseKeeper()

    f = open('./report_'+ hk.datetoday +'.txt', 'a')

    hk.generateFolders()

    ## 2. Download mapping files from servers:
    mapaddresses=hk.get_mappingdata_addresses()

    for the_url in mapaddresses:
        fido = fd.FileDownloader(current_houseKeeper=hk, type="mappings")
        fido.download_file(the_url=the_url)


    ## 3. Unpack files:
    tax_list = ["9606", "10090", "10116"]
    species_list = ["HUMAN_9606", "MOUSE_10090", "RAT_10116"]
    uniprot_files = "_idmapping_selected.tab"
    [fido.unpack_file(hk.get_mapping_files_path() + "/" + i + uniprot_files + ".gz") for i in species_list]


    ## 4. Write versions:
    for the_url in mapaddresses:
        db_raw = the_url.split("/")[-1]
        db_name = re.sub('[^a-zA-Z0-9-_.]', '.', db_raw)
        fido.save_version(db_name)

    ## 5. Join into one file:
    filesin =  hk.get_mapping_files_path() + "/*" + uniprot_files
    filesout = hk.get_mapping_files_path() + "/" + "_".join(species_list) + uniprot_files

    if os.path.exists(filesout):
        subprocess.run("rm " + filesout, shell=True, check=True)

    subprocess.run("cat " + filesin + " >> " +  filesout, shell=True, check=True)

    for taxon in species_list:
        if os.path.exists( hk.get_mapping_files_path() + "/" + taxon + uniprot_files):
            subprocess.run("rm " + hk.get_mapping_files_path() + "/" + taxon + uniprot_files,
                           shell=True, check=True)


    print("## --------- UniProt table: --------------------", file=f)
    ## 6. create tables for human and mouse, uniprot accession 2 entrez id:
    u = UniProtfileParser(tax_list=tax_list, date=hk.datetoday)
    uni2ent = u.make_bimap_uni2entrez()

    ## 7. Read it:
    u2e0 = pd.read_csv(uni2ent, header=0, names=['u', 'e', 't']) 

    print("There are multiple Entrez ids mapped to single Uniprot ids that has to be split before casting Entrez column to integer",
          file = f)

    bp = BaseParser(date=hk.datetoday)
    u2e = bp.split_single_column(u2e0,'e','; ')

    print("Before / After: ",len(u2e0),"/", len(u2e), file = f)


    ## ----------  DATA STATISTICS: ----------

    print("How many genes: ", len(u2e.e.unique()), file = f)

    print("How many proteins: ", len(u2e.u.unique()), file = f)


    ### -------- CREATE LOG FILE WITH MULTI-MAPPINGS: -------------

    u2e_multi = u2e0.loc[u2e0.e.str.contains('; '), :]
    u2e_multi.drop_duplicates(inplace=True)
    u2e_multi.reset_index(inplace=True, drop=True)

    print("Number of multimappings: ", len(u2e_multi), file = f)

    u2e_multi.to_csv("logs/multiple_mappings_of_entrez_id_to_uniprot-from_uniprordb_flatfile.tab", sep='\t', index=False)

    ## ------------------------------------------------------------------------------

    ## 6. Change Entrez ids to integers, strip leading and trailing spaces:
    u2e.loc[:, 'u'] = u2e['u'].str.strip()
    u2e.loc[:,'e'] = u2e['e'].astype(int)
    u2e.loc[:,'t'] = u2e['t'].astype(int)

    ## 7. Save it to file:
    u2e.to_csv(hk.get_mapping_files_path() + "/uniprot_uni2ent.tab", index=False, sep='\t')

    ## 8. Load:
    # u2e = pd.read_table(hk.cleaned_mappers + "/uniprot_uni2ent.tab" )


    print("## --------- NCBI table: --------------------", file = f)
    ## 9. Create tables for human, mouse:
    n = NCBIfileParser(tax_list=tax_list, date=hk.datetoday)

    fido.unpack_file(hk.get_mapping_files_path() + "/gene2accession.gz")

    ### entrez id to gene symbol
    ent2synb = n.make_bimap_entrez2symb()
    ##  entrez id to a mixture of different protein accessions (refseq, uniprot ..)
    ent2prot = n.make_bimap_entrez2protein()


    if os.path.exists(hk.get_mapping_files_path() + "/gene2accession"):
            subprocess.run("rm " + hk.get_mapping_files_path() + "/gene2accession",
                           shell=True, check=True)

    print("## -------- Load two resources, UniProt and NCBI: -----------", file = f)
    ## 10. The two mapping files:
    u2e = pd.read_csv(hk.get_mapping_files_path() + "/uniprot_uni2ent.tab", sep='\t')
    e2u = pd.read_csv(ent2prot, header=0, names = ['t','e','u'])

    ## 11. Cleaning only when single organism
    ## e2u.drop(labels = ['t'], axis = 1, inplace=True)

    ## 12. Remove splice variants:
    e2u['u'] = e2u['u'].map(lambda x: x.rstrip('0123456789').rstrip('.'))

    ## 13. Assertions to check before merging two df:
    # i. columns named in the same way should have the same data types, especially entrez Id one ('e')
    # ii. check for number of ids stored in a single field, i.e.: '100526794; 93034' :
    #    ValueError: invalid literal for long() with base 10: '100526794; 93034'

    e2u['u'] = e2u['u'].str.strip()
    e2u['e'] = e2u['e'].astype(int)
    e2u['t'] = e2u['t'].astype(int)

    print("Compare agreement on types of columns: ", file = f)
    print(type(e2u.t[1]) == type(u2e.t[1]), file = f)
    print(type(e2u.u[1]) == type(u2e.u[1]), file = f)
    print(type(e2u.e[1]) == type(u2e.e[1]), file = f)


    ## 14. Merge:
    nu = pd.merge(e2u, u2e, how="outer", on=["u", "e", "t"])
    # print(len(e2u)+len(u2e))
    # print(len(nu))

    nu.drop_duplicates(inplace=True)
    print("No. of records: ", len(nu), file=f)

    ## ------- DATA STATISTICS: ----------------
    print("Number of entries of merged file: ",len(nu), file = f)

    ## After merge:
    print("Genes mapped: ", len(nu.e.unique()), file = f)
    print("Proteins mapped: ", len(nu.u.unique()), file=f)

    ## 19. Check what are NaN entries:
    print("NaN in data: ", nu[nu.isnull().any(axis=1)], file=f)

    ### 20. SAVE merged:
    nu.to_csv(hk.cleaned_mappers+"/bimap_e2u_ncbi_uniprot.csv", index=False)


    print("## --------- ORTHO table: --------------------", file = f)

    fido.unpack_file(hk.get_mapping_files_path() + "/gene_orthologs.gz")


    f.close()

if __name__ == "__main__":
    main()
