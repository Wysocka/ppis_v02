
# Scripts to create the PPI table

### INPUT DATA FILES ###

+ ``./addresses/data_addresses.txt`` :- provides a list of URL addresses to retrieve PPI datasets 

+ ``./addresses/mappingdata_addresses.txt`` :- ftp/http addresses for the mapping flat files from NCBI and UniProt

+ ``./versions/data_versions.txt`` :- writes the downloaded file name and the date when it was downloaded; lines are appended.

+ ``./mi_terms :- files contains MI terms for filtring PPIs


### CODE:

+ ``main.py`` - gets all done

+ ``make_mappingFiles.py`` - creates mapping files; run by main.py; 

+ ``toolbox/`` - contains utility functions used by the above


### OUTPUT:

+ PPIs are saved out side of this folder

+ ``logs/`` - UniProt Accessins that mapped to mulitple Entrez IDs.



