### HELPER FILES   ###
----------------------

+ ``houseKeeper.py``:- helper class with class methods (you don't have to create its instance to use its methods;
a method call has to be preceded by the class name; caters mainly to get paths to input/output folders and starter files;
thought to deal with very basic organisation of the package environment; add here any method you think would be necessary;


### DOWNLOADING FILES ###
------------------------
+ ``fileDownloader.py`` :- nearly done; downloads and unpacks all necessary files;


### MAPPING FILES ###
---------------------

+ ``baseMapper.py`` :- *to be implemented*; generic class implementing methods generic for mapping functions; might inherit
 ''BaseParser'' class;

+ ``mappingFilesParser.py`` :- a script used to parse flat-files for mapping between identifiers (not sure where it
should belong to, here or to the group bellow)

+ ``directAPIMappers.py`` direct API to UniProt and NCBI dbs --- not ready yet !!



### PARSING FILES ###
---------------------

+ ``baseParser.py``:- class implementing generic methods for parsing; inherited by ``smallTabParsers.py`` classes


