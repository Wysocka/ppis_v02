from pronto import Ontology
import urllib
import os

class MIfile():

    def __init__(self, owlfolder):

        obofile = owlfolder+"/mi.owl"

        if not os.path.exists(obofile):
            miurl = 'https://raw.githubusercontent.com/HUPO-PSI/psi-mi-CV/master/psi-mi.owl'
            urllib.request.urlretrieve(miurl, obofile)

        self.ont = Ontology(obofile)



    def use_directly(self):
        return self.ont
    


    def get_childrenId_of_term(self, term):
        """

        :param term: 

        """
        
        children = tuple(self.ont[term].subclasses(with_self=False))
        childrenIds = [ xx.id for xx in children ]
        
        return childrenIds
    


    def get_childrenName_of_term(self, term):
        """

        :param obofile: 
        :param term: 

        """
        
        children = tuple(self.ont[term].subclasses(with_self=False))
        childrenNames = [ xx.name for xx in children ]
        
        return childrenNames


    def get_direct_interaction_terms(self):

        term = 'MI:0407'
        
        allchildren = self.ont[term].subclasses(with_self=True)
        allchildrenTup = tuple(allchildren)
        
        allchildrenDict = { xx.id: xx.name for xx in allchildrenTup }
        
        return allchildrenDict
 


    def get_physical_association_terms(self):

        term = 'MI:0915'
        allchildren = self.ont[term].subclasses(with_self=True)
        allchildren = allchildren.to_set()
        
        allchildrenTup = tuple(allchildren)
        
        allchildrenDict = { xx.id: xx.name for xx in allchildrenTup }
        
        return allchildrenDict




    def get_association_terms(self):

        term = 'MI:0914'        
        allchildren = self.ont[term].subclasses(with_self=True)
        allchildren = allchildren.to_set()
        
        allchildrenTup = tuple(allchildren)
        
        allchildrenDict = { xx.id: xx.name for xx in allchildrenTup }
        
        return allchildrenDict


    def get_experimental_detection_method_terms(self):

        term = 'MI:0045'
        allchildren = self.ont[term].subclasses(with_self=True)
        allchildren = allchildren.to_set()
        
        allchildrenTup = tuple(allchildren)
        
        allchildrenDict = { xx.id: xx.name for xx in allchildrenTup }
        
        return allchildrenDict

