import os
import re
import sys
from itertools import combinations
import bioservices as bs
import numpy as np
import pandas as pd
from glob import glob
import fileinput
import subprocess
import toolbox.get_mi_terms as gmt
import toolbox.id_mapping_tools as imt
import toolbox.emtools2 as em2
from toolbox.houseKeeper import HouseKeeper
import toolbox.fileDownloader as fd

def dowload_biogrid_intact(datetoday=None):
    """
    RETRIEVE AND UNPACK PPI FILES.

    TODO: In future, remove unpacking step and parse zipfile directly to get columns and taxons.
    TODO: In future get these addresses here.

    :returns: 
    :rtype: 

    """

    ## 1. Create the environment:
    hk = HouseKeeper(datetoday=datetoday)
    ftp = hk.get_local_ftp_path()
    hk.generateFolders()

    ## 2. Get the addresses for PPIs:
    addresses=hk.get_ppiAddresses()

    ## 3. Download the files:
    for the_url in addresses:
        fido = fd.FileDownloader(current_houseKeeper=hk)
        fido.download_file(the_url=the_url)

        pathto = hk.ftp_files + '/' + the_url.rsplit('/', 1)[1]
        fido.unpack_file(path = pathto)
        db_raw = the_url.split("/")[-1]
        db_name = re.sub('[^a-zA-Z0-9-_.]', '.', db_raw)
        fido.save_version(db_name)

    intactf = glob(hk.ftp_files+ "/intact.txt")[0]
    biogf = glob(hk.ftp_files+"/BIOGRID-ALL-*.mitab.txt")[0]

    pathstofiles = [intactf, biogf]

    return pathstofiles


def try_extract(pattern, string):
    try:
        m = pattern.search(string)
        return m.group(0)
    except (TypeError, ValueError, AttributeError):
        return string


def try_extract_empty(pattern, string):
    try:
        m = pattern.search(string)
        return m.group(0)
    except (TypeError, ValueError, AttributeError):
        return ''


def clean_entAB(df):

    p = re.compile(r'^uniprotkb:', re.IGNORECASE)
    df.loc[:, "entA"] = [p.sub('', x) for x in df['entA']]
    df.loc[:, "entB"] = [p.sub('', x) for x in df['entB']]
    df.loc[:, 'entA'] = [x.strip('" ') for x in df['entA']]
    df.loc[:, 'entB'] = [x.strip('" ') for x in df['entB']]

    return df


def clean_symbAB(df):

    p = re.compile(r'^uniprotkb:', re.IGNORECASE)
    df.loc[:, "symbA"] = [p.sub('', x) for x in df['symbA']]
    df.loc[:, "symbB"] = [p.sub('', x) for x in df['symbB']]

    p2 = re.compile(r'\(gene[\s|_]name\)$')
    df.loc[:, "symbA"] = [p2.sub('', x) for x in df['symbA']]
    df.loc[:, "symbB"] = [p2.sub('', x) for x in df['symbB']]

    p3 = re.compile(r'\{.*?\}')
    df.loc[:, "symbA"] = [p3.sub('', x) for x in df['symbA']]
    df.loc[:, "symbB"] = [p3.sub('', x) for x in df['symbB']]


    df.loc[:, 'symbA'] = [x.strip('"') for x in df['symbA']]
    df.loc[:, 'symbB'] = [x.strip('"') for x in df['symbB']]

    df.loc[:, 'symbA'] = [x.strip(' ') for x in df['symbA']]
    df.loc[:, 'symbB'] = [x.strip(' ') for x in df['symbB']]

    return df


def clean_tax(df):

    df.loc[:, "taxA"] = df.taxA.map(lambda x: x.split('|')[0])
    df.loc[:, "taxB"] = df.taxB.map(lambda x: x.split('|')[0])

    p = re.compile(r'^taxid:', re.IGNORECASE)
    df.loc[:, "taxA"] = [p.sub('', x) for x in df['taxA']]
    df.loc[:, "taxB"] = [p.sub('', x) for x in df['taxB']]

    p2 = re.compile(r'\(Homo sapiens\)$', re.IGNORECASE)
    df.loc[:, "taxA"] = [p2.sub('', x) for x in df['taxA']]
    df.loc[:, "taxB"] = [p2.sub('', x) for x in df['taxB']]

    p3 = re.compile(r'\(human\)$', re.IGNORECASE)
    df.loc[:, "taxA"] = [p3.sub('', x) for x in df['taxA']]
    df.loc[:, "taxB"] = [p3.sub('', x) for x in df['taxB']]

    p4 = re.compile(r'\(Mus musculus\)$', re.IGNORECASE)
    df.loc[:, "taxA"] = [p4.sub('', x) for x in df['taxA']]
    df.loc[:, "taxB"] = [p4.sub('', x) for x in df['taxB']]

    p5 = re.compile(r'\(mouse\)$', re.IGNORECASE)
    df.loc[:, "taxA"] = [p5.sub('', x) for x in df['taxA']]
    df.loc[:, "taxB"] = [p5.sub('', x) for x in df['taxB']]

    p6 = re.compile(r'\(Rattus norvegicus\)$', re.IGNORECASE)
    df.loc[:, "taxA"] = [p6.sub('', x) for x in df['taxA']]
    df.loc[:, "taxB"] = [p6.sub('', x) for x in df['taxB']]

    p7 = re.compile(r'\(rat\)$', re.IGNORECASE)
    df.loc[:, "taxA"] = [p7.sub('', x) for x in df['taxA']]
    df.loc[:, "taxB"] = [p7.sub('', x) for x in df['taxB']]

    df.loc[:, 'taxA'] = [x.strip('" ') for x in df['taxA']]
    df.loc[:, 'taxB'] = [x.strip('" ') for x in df['taxB']]

    return df


def clean_pmid(df):

    pattern = re.compile(r'pubmed:[unassigned]*[0-9]*')
    df.loc[:, 'pmid'] = [try_extract(pattern, x) for x in df['pmid']]

    p2 = re.compile(r'pubmed:', re.IGNORECASE)
    df.loc[:, 'pmid'] = [p2.sub('', x) for x in df['pmid']]
    df.loc[:, 'pmid'] = [x.strip('"') for x in df['pmid']]
    df.loc[:, 'pmid'] = [x.strip(' ') for x in df['pmid']]

    return df


def extract_intact_gene_symbol(df):

    pattern = re.compile(r'uniprotkb:[A-Za-z0-9\-]+\(gene name\)')
    pattern2 = re.compile(r'uniprotkb:', re.IGNORECASE)
    pattern3 = re.compile(r'\(gene name\)')

    df.loc[:, 'symbA'] = [try_extract_empty(pattern, x) for x in df['symbA']]
    df.loc[:, 'symbA'] = [pattern2.sub('', x) for x in df['symbA']]
    df.loc[:, 'symbA'] = [pattern3.sub('', x) for x in df['symbA']]
    df.loc[:, 'symbA'] = [x.strip('"') for x in df['symbA']]
    df.loc[:, 'symbA'] = [x.strip(' ') for x in df['symbA']]

    df.loc[:, 'symbB'] = [try_extract_empty(pattern, x) for x in df['symbB']]
    df.loc[:, 'symbB'] = [pattern2.sub('', x) for x in df['symbB']]
    df.loc[:, 'symbB'] = [pattern3.sub('', x) for x in df['symbB']]
    df.loc[:, 'symbB'] = [x.strip('"') for x in df['symbB']]
    df.loc[:, 'symbB'] = [x.strip(' ') for x in df['symbB']]

    return df


def extract_mi_term(df, col):

    pattern = re.compile(r'(MI:[0-9]{4})')
    df.loc[:, col] = [try_extract(pattern, x) for x in df[col]]
    return df


def extract_biogrid_gene_symbol(df):
    """This pattern was checked on command line. The same number of lines (sauff the headline) was returned as
    in original file:

    (base) ➜  ftp_dump grep -E "biogrid:[0-9]+\|entrez gene/locuslink:" BIOGRID-ALL-3.5.181.mitab.txt | wc -l 
    1747191

    (base) ➜  ftp_dump wc -l BIOGRID-ALL-3.5.181.mitab.txt
    1747192 BIOGRID-ALL-3.5.181.mitab.txt

    :param sstring:
    :returns:
    :rtype:

    """
    # biogrid:110131|entrez gene/locuslink:LDHA|entrez gene/locuslink:PIG19
    # biogrid:106961|entrez gene/locuslink:ATIC|entrez gene/locuslink:OK/SW-cl.86


    # sstring = re.sub(r'biogrid:[0-9]+\|entrez gene/locuslink:', "", sstring)
    # sstring = re.sub(r'\|entrez gene/locuslink:[A-Za-z0-9\-\.]+', "", sstring)

    # return sstring


    pattern = re.compile(r'biogrid:[0-9]+\|entrez gene/locuslink:([A-Za-z0-9\-]+)') 
    pattern2 = re.compile(r'biogrid:[0-9]+\|entrez gene/locuslink:', re.IGNORECASE)

    df.loc[:, 'symbA'] = [try_extract_empty(pattern, x) for x in df['symbA']]
    df.loc[:, 'symbA'] = [pattern2.sub('', x) for x in df['symbA']]
    df.loc[:, 'symbA'] = [x.strip('"') for x in df['symbA']]
    df.loc[:, 'symbA'] = [x.strip(' ') for x in df['symbA']]
    df.loc[:, 'symbB'] = [try_extract_empty(pattern, x) for x in df['symbB']]
    df.loc[:, 'symbB'] = [pattern2.sub('', x) for x in df['symbB']]
    df.loc[:, 'symbB'] = [x.strip('"') for x in df['symbB']]
    df.loc[:, 'symbB'] = [x.strip(' ') for x in df['symbB']]

    return df


def clean_method(df):

    df = extract_mi_term(df, col='method')
    df.loc[:, 'method'] = [x.strip(',') for x in df['method']]

    return df


def clean_type(df):

    df = extract_mi_term(df, col='type')
    return df


def clean_sourcedb(df):

    df = extract_mi_term(df, col='sourcedb')
    return df


def seperate_method_pmid(df):

    df1 = em2.split_single_column(df, 'method', '|')
    df2 = em2.split_single_column(df1, 'pmid', '|')
    df3 = em2.split_single_column(df2, 'type', '|')
    return df3


def keep_specific_taxon(df, taxon='9606'):

    taxon = str(taxon)

    return df.loc[df['taxA'].str.contains(taxon) & df['taxB'].str.contains(taxon)]


def select_taxon(filein, fileout, taxon=9606):
    """
    Selects taxon via command line to remove size of the file.
    \ttaxid:(9606|10090|10116).*\ttaxid:(9606|10090|10116)

    :param filein: path to the file
    :param taxon: 
    :returns: path to new file with selected taxon
    :rtype: 

    """

    taxon = "(" + str(taxon) + ")"

    if os.path.exists(fileout):
        os.remove(fileout)
        print("Removed " + fileout + " because it had existed.")

    subprocess.check_call("grep -P '\ttaxid:"+taxon+".*\ttaxid:"+taxon+"' "+ filein+ " > "+ fileout, shell=True)

    print("Taxon-specific file saved here: " + fileout)



def select_columns(column_list, filein, fileout):
    """
    Selects columns indicated in the column list and saves
    the data to the given output file

    option:
    > awk -F'\t' '{print $1,$2,$7,$9,$10,$11,$12,$13}' intact.txt > testawk.tab
    > grep -P '\ttaxid:9606.*\ttaxid:9606'  testawk.tab > testawk_9606.tab

    CHECKED: both give the same results. It also takes some time to produce awk as with python level command.

    :param column_list: list of integers indicating column indexes (starting from 0) which should be removed
    :param taxon_list: numeric ids of selected taxons as list. if it is a single id,
    :param filein: path and name of file of the input file
    :param fileout: path and name of file of the output file
    :return: path to a new file
    """
    filein = glob(filein)
    out = open(fileout, "w")
    for line in fileinput.input(filein):
        if not fileinput.isfirstline():
            field_list = line.split("\t")
            new_field_list = [v for i, v in enumerate(field_list) if i in column_list]
            out.write('\t'.join(new_field_list)+'\n')
    print("The new file with selected columns was written here: "+ fileout)

    out.close()


def select_cols_n_taxon(filein, cols, names, taxon=9606):

    nocol = str(len(cols))
    foldout = filein.rsplit('/', 2)[0] + '/cleaned_ppis/'

    taxnames = taxon.replace('|', '_')

    filename1 = filein.split('.')[0].rsplit('/', 1)[-1] +  "_" + nocol + "cols.tsv"
    fileout1 = foldout + filename1

    filename2 = filein.split('.')[0].rsplit('/', 1)[-1] + '_' + nocol + "cols_" + str(taxnames) + ".tsv"
    fileout2 = foldout + filename2

    select_columns(cols, filein, fileout1)
    select_taxon(fileout1, fileout2, taxon)
 

    outdf = pd.read_csv(glob(fileout2)[0],
                        sep='\t',
                        header=0,
                        names=names,
                        dtype=str)

    return fileout2, outdf


def pars_biogrid_flat(filein,
                      cols=[ 0,1,2,3,6,8,9,10,11,12 ],
                      names=['geneA','geneB', 'symbA', 'symbB', 'method','pmid','taxA','taxB', 'type', 'sourcedb'],
                      taxon='9606', filerepo=sys.stdout):

    # BIOGRID - has mainly direct-interactions, and genetic interactions

    print("\n", file=filerepo)
    print("BioGRID", file=filerepo)
    print("----------------", file=filerepo)

    fileout, bio = select_cols_n_taxon(filein, cols, names, taxon)
    bio.loc[:, 'geneA'] = bio.geneA.str.replace('entrez gene/locuslink:', '')
    bio.loc[:, 'geneB'] = bio.geneB.str.replace('entrez gene/locuslink:', '')

    # bio['symbA'] = [clean_biogrid_gene_name(ss) for ss in bio['symbA']]
    # bio['symbB'] = [clean_biogrid_gene_name(ss) for ss in bio['symbB']]

    bio = extract_biogrid_gene_symbol(bio)

    clean_method(bio)
    clean_tax(bio)
    clean_pmid(bio)
    clean_type(bio)
    clean_sourcedb(bio)

    # bio_2 = seperate_method_pmid(bio_1)

    bio_2 = em2.chainsort_2_columns_in_df(bio, 'symbA', 'symbB', "geneA", "geneB", "taxA", "taxB")

    ## Keep uniform taxon:
    bio_2 = keep_specific_taxon(bio_2, taxon=taxon)

    ## bio_2.drop(columns=['taxA', 'taxB'], inplace=True)

    bio_2.drop_duplicates(inplace=True)
    bio_2.reset_index(inplace=True, drop=True)

    fileout = fileout.rsplit('.')[0] + '_parsed.csv'
    bio_2.to_csv(fileout, index=False)

    bionull = bio_2[bio_2.isnull().any(axis = 1)]
    print("Number of rows which are null: ", len(bionull), file=filerepo)
    print("The finally parsed file was written here: "+ fileout, file=filerepo)
    return bio_2, fileout


def pars_intact_flat(filein,
                     cols=[ 0,1,4,5,6,8,9,10,11,12, 15 ],
                     names=['entA','entB', 'symbA', 'symbB', 'method','pmid','taxA','taxB', 'type', 'sourcedb', 'spoke'],
                     taxon='9606', filerepo=sys.stdout):

    taxon = str(taxon)

    print("\n", file=filerepo)
    print("IntAct", file=filerepo)
    print("----------------", file=filerepo)

    #  FOR THESE WITH complex expansion information (IntAct);
    fileout, intt = select_cols_n_taxon(filein, cols, names, taxon)

    clean_entAB(intt)
    clean_method(intt)
    clean_type(intt)
    clean_tax(intt)
    clean_pmid(intt)
    clean_sourcedb(intt)

    ## Remove information about splice variants:
    intt.loc[:, 'entA'] = intt['entA'].map(lambda x: re.sub(r"-[0-9]", "", x))
    intt.loc[:, 'entB'] = intt['entB'].map(lambda x: re.sub(r"-[0-9]", "", x))
    int_1 = intt.loc[(intt['entA'].map(len) == 6) & (intt['entB'].map(len) == 6)]

    int_1 = extract_intact_gene_symbol(int_1)

    int_2 = em2.chainsort_2_columns_in_df(int_1, "symbA", "symbB", 'entA', 'entB', 'taxA', 'taxB')
    int_2 = int_2.drop_duplicates().reset_index(drop=True)

    ## Keep uniform taxon:
    int_2 = keep_specific_taxon(int_2, taxon=taxon)

    ## int_2.drop(columns=['taxA', 'taxB'], inplace=True)

    fileout = fileout.rsplit('.')[0] + '_parsed.csv'
    int_2.to_csv(fileout, index=False)

    intnull = int_2[int_2.isnull().any(axis = 1)]
    print("Number of rows which are null: ", len(intnull), file=filerepo)
    return int_2, fileout


def report_sourcedb(df, projhome, frepo, mionto):

    ###
    ###    GET COUNTS PER SOURCE:
    ###
    sources = sorted(df['sourcedb'].unique())

    persource = {name: len(df[df.sourcedb == name]) for name in sources}

    onto = mionto.use_directly()

    persourcenmd = {(onto[key].name if 'MI:' in key else key): val for key, val in persource.items()}

    print("\n", file = frepo)
    print("Sources with interaction counts: ", file=frepo)
    for key, val in persourcenmd.items():
        print('- ' + key, ':', val, file=frepo)

    print("Total publication counts: ", len(df['pmid'].unique()), file=frepo)
    print("Publication per source: \n", file=frepo)
    for skey, sval in persource.items():
        pmids = df['pmid'][df['sourcedb'] == skey].unique()
        if 'MI:' in skey:
            print('- ' + onto[skey].name + ': ',  len(pmids), file=frepo)
        else:
            print( '- ' + skey + ': ', len(pmids), file=frepo)


def report_after_merge(dfold, dfnew, filerepo):

    print("Before merge: ", len(dfold), file=filerepo)
    print("After merge: ", len(dfnew), file=filerepo)
    print("Old drop duplicates: ", len(dfold.drop_duplicates()), file=filerepo)
    print("Merged drop duplicates: ", len(dfnew.drop_duplicates()), file=filerepo)
    

    print("Sum of both", len(dfnew) + len(dfold), file=filerepo)


def compare_no_of_pmid(df1, df2, filerepo=sys.stdout):

    print("Studies in first: ", len(df1['pmid'].unique()), file=filerepo)
    print("Studies in second: ", len(df2['pmid'].unique()), file=filerepo)
    print("Sum: ", len(df1['pmid'].unique()) + len(df2['pmid'].unique()), file=filerepo)
    print("Union between both: ", len(set(df1['pmid'].astype('str').unique()).union(set(df2['pmid'].astype('str').unique()))), file=filerepo)


def split_columns(df, col, colA, colB, sep=';'):
    """Split a column (col) in dataframe on a separator into two columns (colA and colB).
    Remove non split column (col). 

    :param df: 
    :param col: name of a column to split
    :param colA: symbA
    :param colB: symbB
    :returns: dataframe with one column split into 2
    :rtype:  dataframe

    """
    # new data frame with split value columns:
    new = df[col].str.split(sep, n = 1, expand = True) 

    # making separate first name column from new data frame:
    df[colA]= new[0]
    df[colB]= new[1]

    # Dropping old Name columns:
    df.drop(columns = [col], inplace=True)
