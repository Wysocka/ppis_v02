from toolbox.baseMapper import BaseMapper

class NCBIfileParser(BaseMapper):

    """
    Class to pars gene2accession.tab file.

    """

    def __init__(self, date, tax_list = ["9606"]):
        self.apprx_name = "*gene2accession"
        self.taxon_column_no = 0
        self.status_column_no = 2
        self.tax_list = tax_list
        super(NCBIfileParser, self).__init__(date)


    def make_bimap_entrez2protein(self):
        """
        Columns to preserve: 1:GENE_ID, 5:protein_accession
        File: gene2accession.tab

        :return:
        """
        col_list={1,5,self.taxon_column_no}
        postfix = "."+'_'.join(self.tax_list)+"_ent2prot_ncbi"

        newer_file = self.make_bimap_from_file(self.tax_list,
                                               col_list,
                                               postfix,
                                               self.apprx_name,
                                               self.taxon_column_no,
                                               self.status_column_no)
        return newer_file


    def make_bimap_entrez2symb(self):

        """
        Columns to preserve: 1:GENE_ID, 15: gene symbol
        File: gene2accession.tab

        :return:
        """

        col_list={1,15,self.taxon_column_no}
        postfix = "."+"_".join(self.tax_list)+"_ent2symb_ncbi"
        newer_file = self.make_bimap_from_file(self.tax_list,
                                               col_list,
                                               postfix,
                                               self.apprx_name,
                                               self.taxon_column_no,
                                               self.status_column_no)
        return newer_file



class UniProtfileParser(BaseMapper):
    """
        2) idmapping_selected.tab
        We also provide this tab-delimited table which includes
        the following mappings delimited by tab:

        1. UniProtKB-AC (*saved)
        2. UniProtKB-ID (*saved)
        3. GeneID (EntrezGene) (*saved)
        4. RefSeq
        5. GI
        6. PDB
        7. GO
        8. UniRef100
        9. UniRef90
        10. UniRef50
        11. UniParc
        12. PIR
        13. NCBI-taxon  (*used) taxon column
        14. MIM
        15. UniGene
        16. PubMed
        17. EMBL
        18. EMBL-CDS
        19. Ensembl
        20. Ensembl_TRS
        21. Ensembl_PRO
        22. Additional PubMed

    """

    def __init__(self, date, tax_list):
        self.tax_list=tax_list
        self.apprx_name = "*_idmapping_selected.tab"
        self.taxon_column_no=12
        self.status_column_no=None
        super(UniProtfileParser, self).__init__(date)


    def make_bimap_uni2entrez(self):
        """
        Parses a flat file from UniProt database to a bimap of UniProt Accessions to Entrez Ids
        Caution: the output file will have Reviewed and Unreviewed/Tremble Accessions. This flat
        file doesn't have this information.
        :return:
        """

        col_list={0,2, self.taxon_column_no}
        postfix = "."+"_".join(self.tax_list)+"_uni2ent_unip"
        new_file = self.make_bimap_from_file(self.tax_list,
                                             col_list,
                                             postfix,
                                             self.apprx_name,
                                             self.taxon_column_no,
                                             self.status_column_no)

        return new_file



    def make_bimap_uniAcc2uniId(self):
        """
        Parses a flat file from UniProt database to a bimap of UniProt Ids to Entrez Ids
        :return:
        """

        col_list={0,1, self.taxon_column_no}
        postfix = "."+"_".join(self.tax_list)+"_uniAcc2uniId_unip"
        new_file = self.make_bimap_from_file(self.tax_list,
                                                  col_list,
                                                  postfix,
                                                  self.apprx_name,
                                                  self.taxon_column_no,
                                                  self.status_column_no)

        return new_file
