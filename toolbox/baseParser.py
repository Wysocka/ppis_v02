import fileinput
import re
from glob import glob
import numpy as np
import os
import pandas as pd

from toolbox.houseKeeper import HouseKeeper
from toolbox.directAPIMappers import DirectAPIMappers


class BaseParser(object):

    """
    Generic class grouping methods applicable in many contexts, both mapping and parsing tasks.
    """

    def __init__(self, date=None):

        self.date=date


    def split_single_column(self, df, target_column, separator):
        '''
        TO BE USED WITH THESE FILES WHICH NEED MAPPING UNIPROT ACC TO ENTREZ

        :param df:  dataframe to be split
        :param target_column: the column containing the values to be split
        :param separator: the symbol used to perform the split

        returns: a dataframe with each entry for the target column separated, with each element moved into a new row.
        The values in the other columns are duplicated across the newly divided rows.
        At the end the method removes duplicated rows
        '''

        def splitListToRows(row,row_accumulator,target_column,separator):
            split_row = row[target_column].split(separator)
            for s in split_row:
                new_row = row.to_dict()
                new_row[target_column] = s
                row_accumulator.append(new_row)
        new_rows = []
        df.apply(splitListToRows,axis=1,args = (new_rows,target_column,separator))
        new_df = pd.DataFrame(new_rows)
        return new_df.drop_duplicates()



    def split_two_columns(self, df, col1, col2, separator):
        """
        The method does the same what split_single_column one but applied to two columns

        :param df: dataframe to split
        :param col1: string - label of the first column
        :param col2: string - label of the second column
        :return: new dataframe with removed duplicated rows
        """
        if isinstance(df[col1][0], str) or isinstance(df[col2][0], str):

            newdf1 = self.split_single_column(df, col1, separator).reset_index(drop=True)
            newdf2 = self.split_single_column(newdf1, col2, separator).reset_index(drop=True)
            return newdf2.drop_duplicates()
        else:
            return df



    def is_field_numeric(self, field):
        """
        Test to check if a field is numeric. Good for taxononmic ids and Entrez Ids
        :param field: a single filed in a column
        :return: True if it is a numeric value (can be cast to integer) or zero if not and rises ValueError
        """

        try:
            f_int = int(field)
            return True
        except ValueError:
            print(field)
            return 0


    def is_filed_MI(self, filed):
        """
        To check if a field is MI: identifier
        :param filed: a single field in a column
        :return: True if yes, zero if not
        """
        if re.search("MI:[0-9]+", filed):
            return True
        else:
            return 0


    def filter_mirrored_duplicates(dataFrame, col1, col2):
        """
        Removes mirrored duplicates

        :param dataFrame: pandas' dataframe
        :param col1:
        :param col2:
        :return:
        """

        df = dataFrame[dataFrame[col1] > dataFrame[col2]]
        to_drop = df.select(lambda x: ((dataFrame[col2] == df[col1].loc[x]) & (dataFrame[col1] == df[col2].loc[x])).any())
        return dataFrame.drop(to_drop.index)



    def get_path_with_input_file(self, file_apprx_name, which='mapping'):
        """

        :param file_apprx_name: part of the mapper file name; unknown part can be replace with asterisk ('*') sign
        :return: path of the file
        """
        hk = HouseKeeper(self.date)

        if which=='mapping':
            in_path = hk.get_mapping_files_path()
        elif which == 'ppi':
            in_path = hk.get_local_ftp_path()

        in_file = glob(os.path.join(in_path,file_apprx_name))

        assert len(in_file)>0, "There is *no file* matching file to the pattern: "\
                                    +file_apprx_name\
                                    +" in the folder: "+ in_path


        assert len(in_file)<2, "There are *more than one* matching file to the pattern: "\
                                    +file_apprx_name\
                                    +" in the folder: "+ in_path


        return in_file[0]



    def is_uniprotAc(self, string):
        """
        Predicate class checking if the uniprot identifier belongs to the Accession type or Id type (ABCD_TAXON)
        :param string:  type of UniProt identifiers as a string
        :return: True if it's Accession, False if Id, Exception if neither
        """

        if '_' in string:
            return False
        elif re.match('[A-Z][0-9]', string):
            return True
        else:
            raise Exception("It's neither of UniProts :", string)


    def get_pubmed_term(self, field_str, mitab_line):
        """
        Extract a pubmed id given a field string (column number corresponding to PubMed Ids should be always the same)
        :param field_str: field string
        :param mitab_line: newly created line to be written in a new file
        :return: input mitab_line with added pubmed id, if exists; if not, the mitab line given on an input;
        """

        if re.match('pubmed', field_str):
            current_terms = field_str.split("|")
            for term in current_terms:
                     if re.match("pubmed", term):
                         mitab_line= mitab_line+"\t"+term.strip('pubmed:')

        return mitab_line


    def get_tax_ids(self, line_list, mitab_line):
        """

        :param line_list: nth line of an input file divided into a list of fields
        :param mitab_line: newly and gradually created line with extracted information from different line fields
        :return: input mitab_line with added taxonomic id, if exists; if not, the mitab line given on an input;
        """
        taxid_count=0
        for field in line_list:
            if re.match('taxid:', field):
                if taxid_count == 2:
                    pass
                else:
                    mitab_line = mitab_line+"\t"+field.strip('taxid:').split('|')[0].split('(')[0]
                    taxid_count+=1
        return mitab_line


    def get_mi_term(self, line_list, mi_term, special, mitab_line):
        """
        Extract MI term id corresponding to an experimental method given a list of fields.

        :param line_list: nth line of an input file divided into a list of fields
        :param mi_term: tagging/key word, specific to a database, that the id of experiment has to be extracted from
        :param special: surrounding separators, specific to a database  that the id has to be extracted from
        :param mitab_line: newly and gradually created line with extracted information from different line fields
        :return: input mitab_line with added info about experiment (MI:), if exists; if not, the mitab line given on an input;
        """

        psimi_count = 0
        experiment=line_list[6]
        if experiment != '-' and re.match(mi_term, experiment):
            if psimi_count ==1 :
                pass
            else:
                ## why do we take only first MI id ????
                mitab_line = mitab_line+"\t"+experiment.split(special[0])[0].split(special[1])[0]
                psimi_count+=1

            return mitab_line
        else:
            return mitab_line+"\t"+"0"


    def get_mi_multi_terms(self):
        pass



    def select_columns(self, column_list, filein, fileout):

        """
        Selects columns indicated in the column list and saves
        the data to the given output file
        :param column_list: list of integers indicating column indexes (starting from 0) which should be removed
        :param filein: path and name of file of the input file
        :param fileout: path and name of file of the output file
        :return: path to a new file
        """

        filein = glob(filein)
        new_file_path = fileout
        out = open(new_file_path, "w")
        for line in fileinput.input(filein):
            if not fileinput.isfirstline():
                field_list = line.split("\t")
                new_field_list = [v for i, v in enumerate(field_list) if i in column_list]
                out.write('\t'.join(new_field_list)+'\n')
        print("The new file was written here: "+ new_file_path)

        out.close()
        return new_file_path


    def remove_duplicates_missing(self, path_to_file):
        """
        Removes duplicated entries and removes a row if any value is missing, either Entrez id, UniProt
        or taxononomic id.

        :param path_to_file:
        :return: dataframe
        """

        tab = pd.read_csv(path_to_file, header=0, names=['a', 'b', 'c'], sep='\t')
        tab.replace('-',np.NaN, inplace=True )
        tab.dropna(axis='index', how='any', inplace=True)
        tab.drop_duplicates(inplace=True)

        return tab


    def get_entrez_interactors(self, line_list):
        """
        A method to be used if first two ids (columns) of interactions are known to be Entrez Ids;
        Issue to address (and explains the existence of get_entrez_interactions_2 method):
        there are mitab files in such databases like HIPPIE, that even though interactors are encoded with
        Entrez Id, first or second interactor might be missing. But, the following two columns have UniProt Ids
        (not Accessions!!) that can be used to find Entrez Ids via different databases.

         See example of this issue: ``ppi_data/cleaned_ppis/log_hippie_new3.txt``; 631 interactions are dropped
         without trying to mapped them to Entrez Ids.


        :param line_list: nth line of an input file divided into a list of fields
        :return: a string of combined ids of two interactors specified in Entrez Ids, separated by a tab,
        """
        genes = []
        if re.match('^[0-9]',line_list[0].strip('entrez gene:')):
            genes.append(re.sub('-[0-9]','',line_list[0].strip('entrez gene:')))

        if re.match('^[0-9]',line_list[1].strip('entrez gene:')):
            genes.append(re.sub('-[0-9]','',line_list[1].strip('entrez gene:')))

        if len(genes)==2:
            return genes[0]+"\t"+genes[1]

        else:
            print("There is a problem with Entrez Id with this line: " + ', '.join(line_list))
            return 0


    def get_entrez_interactors_2(self, line_list):
        """
        A method to be used if first two ids (columns) of interactions are known to be Entrez Ids;

        For more explanations see notes to get_entrez_interactors method above;.

        It's in the process of construction (it's not yet where it should be).

        :param line_list: a line of an input file divided into a list of fields
        :return: a string of combined ids of two interactors specified in Entrez Ids, separated by a tab,
        """

        genes = []
        if re.match('^[0-9]',line_list[0].strip('entrez gene:')):
            genes.append(re.sub('-[0-9]','',line_list[0].strip('entrez gene:')))
        else:
            genes.append('-')

        if re.match('^[0-9]',line_list[1].strip('entrez gene:')):
            genes.append(re.sub('-[0-9]','',line_list[1].strip('entrez gene:')))
        else:
            genes.append('-')

        if  len(genes[0])!=1 and len(genes[1])!=1:
            return genes[0]+"\t"+genes[1]
        elif len(genes[0])==1 and len(genes[1])!=1:

            alt1=line_list[2].strip('uniprotkb:')
            # if self.is_uniprotAc(alt1):
            #     ua = UniprotACCMapper(alt1, taxon=None)
            #     try:
            #         alt1_entrez= ua.uniprotAcc_entrezId()
            #         return alt1_entrez+"\t"+genes[1]
            #     except Exception,e:
            #         print(e)
            #         pass
            #
            # else:
            #     ui = UniprotIdMapper(alt1)
            #     try:
            #         alt1_entrez= ui.uniprotId_entrezId()
            #         return alt1_entrez+"\t"+genes[1]
            #     except Exception,e:
            #         print(e)

            alt1_entrez = DirectAPIMappers().uniprotId_entrezId_UniP(alt1)
            print(alt1_entrez)
            return alt1_entrez+"\t"+genes[1]


        elif len(genes[0])!=1 and len(genes[1])!=1:

            alt2 = line_list[3].strip('uniprotkb:')
            #
            # if self.is_uniprotAc(alt2):
            #     ua = UniprotACCMapper(alt2)
            #     try:
            #         alt2_entrez= ua.uniprotAcc_entrezId()
            #         return genes[1]+"\t"+ alt2_entrez
            #     except Exception,e:
            #         print(e)
            #         pass
            #
            # else:
            #     ui = UniprotIdMapper(alt2)
            #     try:
            #         alt2_entrez= ui.uniprotId_entrezId()
            #         return genes[0]+"\t"+alt2_entrez
            #     except Exception,e:
            #         print(e)
            #         pass
            alt2_entrez = DirectAPIMappers().uniprotId_entrezId_UniP(alt2)
            print(alt2_entrez)
            return genes[0]+"\t"+alt2_entrez
        else:
            print("There are more than two interactors in a single line with Entrez Id. Check this line: " + ', '.join(line_list))
            pass



    def get_uniprotAc_interactors(self, line_list):
        """
        A method to be used if first two ids (columns) of interactions are known to be UniProt Accessions
        :param line_list: nth line of an input file divided into a list of
        :return: a string of combined ids of two interactors, mapped from UniProt Accessions to Entrez Ids, separated by
          a tab, if and only if there exists such two Entrez Ids that map to original UniProt Accessions, else pass
        """

        #where does this dictionary come from?
        uniprotAc2entrezId = dict

        proteins = []
        #we check if the first ID is a uniprot ID
        #it should start with a letter followed by an umber
        if re.match('^[A-Z][0-9]',line_list[0].strip('uniprotkb:')):
            #if so we extract the EntrezID (without letters)
            uniprot_ac = re.sub('-[0-9]','',line_list[0].strip('uniprotkb:'))
            #we now obtain the corresponding EntrezID (based on the given uniprot ID)
            try:
                proteins.append(uniprotAc2entrezId[uniprot_ac])
            except:
                pass
        if re.match('^[A-Z][0-9]',line_list[1].strip('uniprotkb:')):
            uniprot_ac = re.sub('-[0-9]','',line_list[1].strip('uniprotkb:'))
            try:
                proteins.append(uniprotAc2entrezId[uniprot_ac])
            except:
                pass
        if len(proteins)==2:
            return proteins[0]+"\t"+proteins[1]


    #adding a function that does the same as above, but only with one given value
    #this makes the process more modular
    def get_uniprotAc_interactors_1(self, line_list):
        """
        A method to be used if identifiers are given as UniProt Accessions
        :param line_list: nth line of an input file divided into a list of
        :return: EntrezID mapped from UniProt Accessions
            if it can't be mapped returns "-"
        """

        # #where does this dictionary come from?
        # uniprotAc2entrezId = dict
        #
        # EntrezID = []
        # #we check if the first ID is a uniprot ID
        # #it should start with a letter followed by an umber
        # if re.match('^[A-Z][0-9]',line_list[0].strip('uniprotkb:')):
        #     #if so we extract the EntrezID (without letters)
        #     uniprot_ac = re.sub('-[0-9]','',line_list[0].strip('uniprotkb:'))
        #     #we now obtain the corresponding EntrezID (based on the given uniprot ID)
        #     try:
        #         EntrezID.append(uniprotAc2entrezId[uniprot_ac])
        #     except:
        #         EntrezID = "-"
        #         # pass
        # # if re.match('^[A-Z][0-9]',line_list[1].strip('uniprotkb:')):
        # #     uniprot_ac = re.sub('-[0-9]','',line_list[1].strip('uniprotkb:'))
        # #     try:
        # #         proteins.append(uniprotAc2entrezId[uniprot_ac])
        # #     except:
        # #         pass
        # # if len(proteins)==2:
        # #     return proteins[0]+"\t"+proteins[1]
        #     return EntrezID
