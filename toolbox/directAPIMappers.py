#from baseMapper import BaseMapper
from bioservices import EUtils
from bioservices import UniProt


class DirectAPIMappers(object):

    def prep_input(self,lista):
        return ','.join(lista)


    def uniprotAc_entrezId_UP(self, list_or_str):
        """
        UniProt to Entrez via bioservices.UniProt API connection

        """
        if isinstance(list_or_str, list):
            string = self.prep_input(list_or_str)

        else:
            assert isinstance(list_or_str, str), "The input should be a list of ids or a string with a single id."
            string = list_or_str

        up = UniProt()
        res = up.mapping(fr="ACC", to="P_ENTREZGENEID", query=string)
        res2 = [x.encode('utf-8') for v in res.values() for x in v]

        if len(res2)==0:
            raise Exception("No matching Entrez ID")
        elif len(res2)==1:
            print(res2)
            return res2[0]
        elif len(res2)>1:
            print(res2)
            return res2
        else:
            raise Exception("Weired value of the result: "+str(res2))


    def uniprotAc_entrez_NCBI(self, uniprot_ac_str):
        pass
        #eu = EUtils()

    def uniprotId_entrezId_UP(self, string):

        up = UniProt()
        res = up.mapping(fr="ID", to="P_ENTREZGENEID", query=string)


        if len(res.items()) == 1:
            return res.values()[0][0].encode("utf-8")
        elif len(res.items()) > 1:
            return ', '.join(res.values()[0]).encode("utf-8")
        else:
            return 0