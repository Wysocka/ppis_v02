import datetime
import os


class HouseKeeper(object):
    """
    Sets up the environment with a current date and folders.

    """

    @classmethod
    def CheckFileIfExists(cls, file_to_check):
        """
        A test class method
        Check if exist data_addresses
        if not raise exception that these files should be where all the scripts are."
        :return:
        """
        if not os.path.exists(file_to_check):
            raise Exception("No "+file_to_check+" file. It should be located where all the scripts are.")

    def __init__(self, datetoday=None):
        """

        Set the date for today if the date is not given.
        :return:
        """
        if datetoday==None:
            self.datetoday = datetime.date.today().isoformat()

        else:
            self.datetoday = datetoday

        upperdir = os.path.dirname(os.getcwd())
        self.data_folder=os.path.join(str(upperdir),"ppi_data_"+self.datetoday)
        self.lite_db = os.path.join(self.data_folder,"lite_db")
        self.ftp_files= os.path.join(self.data_folder,"ftp_dump")
        self.temp_folder=os.path.join(self.data_folder,"temp")
        self.cleaned_mappers = os.path.join(self.data_folder,"cleaned_mappers")
        self.separate_mappers = os.path.join(self.ftp_files,"mapping_files")
        self.cleaned_ppis = os.path.join(self.data_folder, "cleaned_ppis")
        self.file_addresses = "./addresses/data_addresses.txt"
        self.file_versions = "./versions/data_versions.txt"
        self.mapping_data = "./addresses/mappingdata_addresses.txt"

    def generateFolders(self):
        """
        Creates a folder structure for the output data if it doesn't find it.
        :return: path to current directory where all folders with outputs should be .
        """

        if not os.path.exists(self.data_folder):
            os.mkdir(self.data_folder)
            print("../ppi_data_"+self.datetoday+" was created")
        if not os.path.exists(self.lite_db):
            os.mkdir(self.lite_db)
            print("../ppi_data_"+self.datetoday+"/lite_db was created")
        if not os.path.exists(self.ftp_files):
            os.mkdir(self.ftp_files)
            print("../ppi_data_"+self.datetoday+"/ftp_dump was created")
        if not os.path.exists(self.temp_folder):
            os.mkdir(self.temp_folder)
            print("../ppi_data_"+self.datetoday+"/temp was created")
        if not os.path.exists(self.cleaned_mappers):
            os.mkdir(self.cleaned_mappers)
            print("../ppi_data_"+self.datetoday+"/cleaned_mappers was created")
        if not os.path.exists(self.separate_mappers):
            os.mkdir(self.separate_mappers)
            print("../ppi_data_"+self.datetoday+"/ftp_dump/mapping_files was created")
        if not os.path.exists(self.cleaned_ppis):
            os.mkdir(self.cleaned_ppis)
            print("../ppi_data_"+self.datetoday+"/cleaned_ppis was created")

        print("All folders exists.")


    def get_ppiAddresses(self):
        """
        :return: list of url database addresses specified in data_addresses.txt file
        """
        self.CheckFileIfExists(self.file_addresses)

        with open(self.file_addresses, 'r') as f:
            adlist=f.read()
            f.close
        return [i for i in adlist.split("\n") if i]



    def get_local_ftp_path(self):
        """

        :return: path to file with all ftp files
        """
        return self.ftp_files



    def get_local_temp_path(self):
        """

        :return: path to file with temp files. Empty right now but with  implementation of the class TimeStampChecker
        it might be necessary to read the timestamp of zipped files  (first load, then  unpack  and then read the timestamp).
        Unless we'll find another method to check if two files are the same.
        """
        return self.temp_folder


    def get_local_sqlite_path(self):
        """

        :return: path to created/existing sqllite database
        """
        return self.lite_db


    def read_file_with_versions(self):
        """
        Checks before if the file exists.
        :return: reads file with information about downloaded file names and dates it was done.
        """
        self.CheckFileIfExists(self.file_versions)
        with open(self.file_versions, "r") as fn:
            return fn.readlines()


    def get_file_with_versions(self):
        """
        Class method; Checks before if the file exists.
        :return: path to data_versions.txt file which has all information about names and dates of file downloads.
        """
        self.CheckFileIfExists(self.file_versions)
        return self.file_versions


    def get_mapping_files_path(self):
        return self.separate_mappers


    def get_mappingdata_addresses(self):
        """
        :return: list of url database addresses specified in mappingdata_addresses.txt file
        """
        self.CheckFileIfExists(self.mapping_data)

        with open(self.mapping_data, 'r') as f:
            adlist=f.read()
            f.close

        ## Remove empty string:
        return [i for i in adlist.split("\n") if i]
