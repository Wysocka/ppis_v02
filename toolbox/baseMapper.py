import fileinput
import re
from glob import glob
import os
import pandas as pd
import numpy as np
from toolbox.baseParser import BaseParser

class BaseMapper(BaseParser):

    """
        Method is necessary to implement for parsing large files from NCBI (e.g.: 2015-12-08_gene2accession) and UniProt;
        Select only these taxons;
        grep -E '^9606|^10116|^10090' 2015-12-08_gene2accession_selected_cols.tab > 2015-12-08_gene2accession_selected_cols_HMR.tab

    """

    def __init__(self, date):
        super(BaseMapper, self).__init__(date)


    def select_columns_taxons(self, column_list, taxon_list, filein, fileout, taxon_column_no, status_column_no=None):

        """
        Selects columns indicated in the column list and taxons from the taxon_list and saves
        the data to the given output file. If a file is from NCBI then it will check the status of an id, else only
        taxon number is considered.

        Types of possible statuses // awk -F '\t' '{print $2}' 2015-12-08_gene2accession.9606_ent2symb_ncbi.tab | sort | uniq -c | sort -nr :
            1070540
             560264 -
             224339 MODEL
             114001 REVIEWED
              90959 VALIDATED
              56860 INFERRED
              14472 PROVISIONAL
               7465 SUPPRESSED
               1808 PREDICTED
                372 NA
        According to the NCBI website about RefSeq statueses meanings (http://www.ncbi.nlm.nih.gov/refseq/about/)
        all other than VALIDATED and REVIEWED positions will be ignored (these ones were verified by human).


        :rtype: object
        :param column_list: list of integers indicating column indexes (starting from 0) which should be removed
        :param taxon_list: numeric ids of selected taxons as list. if it is a single id,
        :param filein: path and name of file of the input file
        :param fileout: path and name of file of the output file
        :param taxon_column_no: integer to indicate column number where taxon information is stored
        :param status_column_no: if the flat file is from NCBI status_column_no will have different than None value
         then method will check what STATUS an id has, else it will filter a line based on taxon number only.

        :return: path to a new file
        """
        taxon_list2 = ['\\b'+i+'\\b' for i in taxon_list]
        taxon_str = "|".join(taxon_list2)
        print("Selecting columns only for the following taxon(s): "+taxon_str)

        new_file_path = fileout
        out = open(new_file_path, "w")

        for line in fileinput.input(filein):
            if not fileinput.isfirstline():
                field_list = line.split("\t")

                if re.search(taxon_str, field_list[taxon_column_no]):
                    status_str =  'REVIEWED|VALIDATED'
                    if status_column_no!=None and re.match(status_str, field_list[status_column_no]):
                        new_field_list = [v for i, v in enumerate(field_list) if i in column_list]
                        out.write('\t'.join(new_field_list)+'\n')
                    else:
                        new_field_list = [v for i, v in enumerate(field_list) if i in column_list]
                        out.write('\t'.join(new_field_list)+'\n')


        out.close()
        print("The new file was written here: "+ new_file_path)

        return new_file_path


    def make_bimap_from_file(self, taxon_list, column_list,
                             postfix, apprx_name, taxon_column_no, status_column_no):
        """

        :param taxon_list:
        :param column_list:
        :param postfix:
        :param apprx_name:
        :param taxon_column_no:
        :param status_column_no:
        :return:
        """
        mapperfilepath = self.get_path_with_input_file(apprx_name, which="mapping")

        new_file = self.select_columns_taxons(column_list,taxon_list,
                                              mapperfilepath,
                                              mapperfilepath+postfix,
                                              taxon_column_no,
                                              status_column_no)

        print("Removing duplicates and missing values")
        df = self.remove_duplicates_missing(new_file)
        newer_file = new_file+".cleaned.csv"
        df.to_csv(newer_file, header=False, index=False)
        print("File: " + newer_file+" was saved." )

        return newer_file
