import urllib, zipfile, tarfile, gzip
import re
from toolbox.houseKeeper import HouseKeeper
import datetime
import os
import subprocess

class FileDownloader(object):

    """
    Class enclosing methods that cater to download and unpack files given the url address
    """



    def __init__(self, current_houseKeeper=None, type="ppi"):
        """

        :param current_houseKeeper: which houseKeeper
        :param type: change the path, depended if it is "ppi" of "map"
        :returns:
        :rtype:

        """
        if current_houseKeeper==None:
            self.hk = HouseKeeper()
        else:
            self.hk = current_houseKeeper

        self.versions_file = self.hk.get_file_with_versions()
        self.datetoday = self.hk.datetoday
        if type=="ppi":
            self.to_folder = self.hk.ftp_files
        else:
            self.to_folder = self.hk.get_mapping_files_path()



    def is_Not_Found(self, path_to_file):
        """
        Method used by the download_file method.
        Reads file which was downloaded and unpacked to check if a server.
         returned a message in a xml format that a given address wasn't found.
        :param path_to_file: path to downloaded file
        :return: boolean True or False ; if True - the url address is not found, else it's OK.
        """
        with open(path_to_file, 'r') as fn:
            r = fn.read()
            return "404 Not Found" in r



    def retrive_url(self, the_url, db_name):
        """
        Internal method used by the download_file method.
        :param the_url: url address where the file to be downloaded is
        :param db_name: database name which is a file name of downloaded file
        :return: tuple, path of the downloaded file  and header of the queried page in the form of an
        http.client.HTTPMessage instance (for more info about http header see this: http://www.cs.tut.fi/~jkorpela/http.html)
        """

        print("Retrieving data: " + db_name)
        filename, hdrs = urllib.request.urlretrieve(the_url, self.to_folder+"/"+db_name)
        print("The result is -->", filename, hdrs)
        return filename, hdrs


    def save_version(self, db_name):
        """
        Method used by the download_file method.
        :param db_name: database name which is also a name of the file with data from this database
        :return: writes a name and a date of the file indicated by the input value into a file called data_versions.txt;
        then prints to the console what it happened.
        """
        try:
            with open(self.versions_file, "a+") as fn:
                fn.write("\n--------------------------------------------------------------\n"
                         + db_name +" -- "+ self.datetoday + "\n")
            print("The file timestamp was saved in data_versions.txt")
        except Exception as ex:
            print("Couldn't save version: %s" % (ex))



    # def set_compression_type(self, path):
    #     """
    #     Method used by the download_file method.
    #     'Elif' checks which type of the compression was used to pack a file based on its extension.
    #     Options that are checked for: .zip, .gz,
    #     gz, .tbz, bz2, gzip;
    #     :param path:
    #     :return: name of the function to be used and mode type as an argument to this function
    #     """
    #     if path.endswith('.zip'):
    #         return zipfile.ZipFile, 'r'
    #     elif path.endswith('gz') or path.endswith('.bz2') or path.endswith('.tbz'):
    #         return tarfile.open, 'r:*'
    #     else:
    #         print("It's not 'zip', 'gz', nor 'bz2. Trying gzip just now ...")

    #         try:
    #             with gzip.open(path, 'r') as infile:
    #                 with open(path+".txt", 'w') as outfile:
    #                     for line in infile:
    #                         outfile.write(line)
    #                         print("The file " +path+ " was unpacked with gzip.")
    #         except Exception as e2:
    #             print("The file "+path+ "cannot be extrected because ", e2)



    def get_folderpath_from_filepath(self, path):
        """
        Internal method used by the download_file method.
        Separates a file name from a path to it.
        :param path: file path
        :return: path to folder where the file can be found
        """

        path_list = path.split("/")
        return "/".join(path_list[:-1])


    # def unpack_file(self, path):
    #     """
    #     Method used by the download_file method.
    #     :param path: file to be unpacked
    #     :return: prints the message of success into the console followed by a path to the unpacked file
    #     """

    #     opener, mode = self.set_compression_type(path)
    #     file = opener(path, mode)
    #     print("Extracting file: ", path)

    #     local_ftp_folder= self.get_folderpath_from_filepath(path)
    #     file.extractall(path=local_ftp_folder, )
    #     file.close()
    #     print("Succeeded ! I'm done with file ", path )

    def unpack_file(self, path):
        """Gunzips file and selects

        :param path:
        :returns:
        :rtype:

        """
        try:
            if path.endswith('.zip'):
                check = subprocess.check_call(["unzip", path, "-d", os.path.dirname(path)])
            elif path.endswith('.gz'):
                check = subprocess.check_call(["gunzip","-k", path])

            print("Output message: ", check)
        except Exception as ex:
            print("Couldn't unpack: %s" % (ex))


    def download_file(self, the_url):
        """
        Main executing function of the class FileDownloader.
        Retrieves url, if the content of returned file is that the
        address wasn't found, raises an IOError, saves name and download date
        into a separate file called 'data_versions.txt'
        and finally unpacks the file into the same folder where it was downloaded, i.e.: /ftp_dump/
        :param the_url: url address of a file to download
        :return: name of downloaded file with its path if all other procedures were successful.
        """

        db_raw = the_url.split("/")[-1]
        db_name = re.sub('[^a-zA-Z0-9-_.]', '.', db_raw)
        db_path = os.path.join(self.to_folder, db_name)

        if not os.path.exists(db_path):
            try:
                path, hdrs = self.retrive_url(the_url, db_name)

                if self.is_Not_Found(path):
                    raise IOError("It's wrong url address - file " + path + " wasn't found")
                else:
                    print("Downloaded file.")
                    self.save_version(db_name)
                    print("Saved file version")
                    self.unpack_file(path)
                    print("Unpacked file.")
                    return path

            except Exception as e:
                print("Problem with %r in %r because of: %s" % (the_url, self.to_folder, e))
        else:
            print("Mapping file "+db_name+" already exists.")
            return db_path
