import socket
import sys
from functools import reduce
from time import sleep

import bioservices as bs
import toolbox.emtools2 as em2
import pandas as pd
from bioservices import HGNC

socket.setdefaulttimeout(120)

def check_mapping_anomalies(df, col='Gene ID' ):
    return df[(df[col] == '-') | (df[col].str.contains('//')) | (df[col] == '')] 


def get_reviewed_uniprot_acc(taxon='9606'):
    """Get the list of reviewed UniProtKB Accessions. Useful to filter out unstable unreviewed accessions
    that are return in excess by BioDbNet.

    :param taxon: String if not, it casts it to string 
    :returns: 
    :rtype: dataframe

    """

    
    u = bs.UniProt(verbose=True)
    revUniProt_raw = u.search('reviewed:yes AND organism:' + str(taxon), 
                      frmt="tab", columns="id")

    revUniProt = revUniProt_raw.split('\n')[1:]
    return revUniProt


def filter_df_with_reviewed_uniprot_acc(df, unicol = 'UniProt Accession', taxon='9606'):

    dfcopied = df.copy()
    revlist = get_reviewed_uniprot_acc(taxon=taxon)
    rev_df = dfcopied[dfcopied[unicol].isin(revlist)]
    return rev_df


def get_acc2id(uniprot_list, taxon=9606, start_iter=0, max_iter=5):
    """
    ###########[        MAPPING Uniprot to Entrez via biodbnet  ]#############
    :param uniprot_list:
    :param taxon:  specify taxon (int value);  default is 9606
    :param max_iter:
    :return: dataframe with uniprot, enrez columns
    """

    try:
        t = bs.BioDBNet()

        output_db = 'Gene ID'
        input_db ='UniProt Accession'
        res = t.db2db(input_db,output_db, uniprot_list, taxon=taxon)

        print("---------------------------")
        print("Show anomalies in mapping: \n", check_mapping_anomalies(df=res, col=output_db))
        print("---------------------------")

        res.reset_index(inplace=True)
        return res

    except Exception as e:
        print(e)
        if start_iter < max_iter:
            sleep(1)
            get_acc2id(uniprot_list, taxon=taxon, start_iter=start_iter+1, max_iter=max_iter)
        else:
            return "After 10 iterations it's still not working. Increase max_iter parameter."


def get_id2acc(geneid_list, taxon=9606, start_iter=0, max_iter=5):
    """
    ###########[        MAPPING Uniprot to Entrez via biodbnet  ]#############
    :param uniprot_list:
    :param taxon:  specify taxon (int value);  default is 9606
    :param max_iter:
    :return: dataframe with uniprot, enrez columns
    """

    try:
        t = bs.BioDBNet()

        input_db = 'Gene ID'
        output_db ='UniProt Accession'
        res = t.db2db(input_db,output_db, geneid_list, taxon=taxon)

        print("---------------------------")
        print("Show anomalies in mapping: \n", check_mapping_anomalies(df=res, col=output_db))
        print("---------------------------")

        res.reset_index(inplace=True)
        resplit = em2.split_single_column(df=res,
                                          separator='//',
                                          target_column='UniProt Accession')

        revones = filter_df_with_reviewed_uniprot_acc(resplit, taxon)
        return revones

    except Exception as e:
        print(e)
        if start_iter < max_iter:
            sleep(1)
            get_id2acc(geneid_list, taxon=taxon, start_iter=start_iter+1, max_iter=max_iter)
        else:
            return "After 5 iterations it's still not working. Increase max_iter parameter."


def get_symb2id(gene_list, taxon=9606, iter=0):
    try:
        t = bs.BioDBNet()

        input_db = 'Gene Symbol'
        output_db = 'Gene ID'

        res = t.db2db(input_db, output_db, input_values=gene_list, taxon=taxon)

        print("---------------------------")
        print("Show anomalies in mapping: \n", check_mapping_anomalies(df=res, col=output_db))
        print("---------------------------")

        res.reset_index(inplace=True)
        return res

    except Exception as e:
        print(e)
        while iter < 5:
            get_symb2id(gene_list, taxon, iter+1)


def get_id2symb(gene_list, taxon=9606, iter=0):

    try:
        t = bs.BioDBNet()

        output_db = 'Gene Symbol'
        input_db = 'Gene ID'

        res = t.db2db(input_db, output_db, input_values=gene_list, taxon=taxon)

        print("---------------------------")
        print("Show anomalies in mapping: \n", check_mapping_anomalies(df=res, col=output_db))
        print("---------------------------")

        res.reset_index(inplace=True)
        return res

    except Exception as e:
        print(e)
        while iter < 5:
            get_symb2id(gene_list, taxon, iter+1)


def get_symb2acc(genes, taxon=9606, iter=0):
    """Recommended to start from Gene Symbols and go to UniProt Accessions

    :param genes: list
    :param taxon: integer
    :param iter: 
    :returns: dataframe
    :rtype: dataframe

    """

    try:
        t = bs.BioDBNet()

        input_db = 'Gene Symbol'
        output_db ='UniProt Accession'
        res = t.db2db(input_db, output_db, genes, taxon=taxon)

        print("---------------------------")
        print("Show anomalies in mapping: \n", check_mapping_anomalies(df=res, col=output_db))
        print("---------------------------")

        res.reset_index(inplace=True)
        resplit = em2.split_single_column(df=res,
                                          separator='//',
                                          target_column='UniProt Accession')

        revones = filter_df_with_reviewed_uniprot_acc(resplit, taxon)
        return revones

    except Exception as e:
        print( e)
        while iter < 5:
            get_symb2acc(genes, taxon, iter+1)  


def get_symb2ensembl(genes, taxon=9606, iter=0):
    """
    It doesn't matter if it is Gene ID or Symbol to map it to Ensembl.

    :param genes: 
    :param taxon: 
    :param iter: 
    :returns: 
    :rtype: 

    """
    try:
        inputdb = 'Gene Symbol'
        outputdb = 'Ensembl Gene ID'

        b = bs.BioDBNet(verbose=True)

        res = b.db2db(input_db=inputdb, output_db=outputdb, input_values=genes, taxon=taxon)

        print("---------------------------")
        print("Show anomalies in mapping: \n", check_mapping_anomalies(df=res, col=outputdb))
        print("---------------------------")

        res.reset_index(inplace=True)

        return res

    except Exception as e:
        print(e)
        while iter < 5:
            get_symb2ensembl(genes, taxon=taxon, iter=iter+1)


def get_ensembl2id(genes, taxon=9606, iter=0):
    """
    It doesn't matter if it is Gene ID or Symbol to map it to Ensembl.

    :param genes: 
    :param taxon: 
    :param iter: 
    :returns: 
    :rtype: 

    """
    try:
        inputdb = 'Ensembl Gene ID'
        outputdb = 'Gene ID'

        b = bs.BioDBNet(verbose=True)

        res = b.db2db(input_db=inputdb, output_db=outputdb, input_values=genes, taxon=taxon)

        print("---------------------------")
        print("Show anomalies in mapping: \n", check_mapping_anomalies(df=res, col=outputdb))
        print("---------------------------")

        res.reset_index(inplace=True)

        return res

    except Exception as e:
        print(e)
        while iter < 5:
            get_symb2ensembl(genes, taxon=taxon, iter=iter+1)



def get_ensembl2symb(genes, taxon=9606, iter=0):
    """
    It doesn't matter if it is Gene ID or Symbol to map it to Ensembl.

    :param genes: 
    :param taxon: 
    :param iter: 
    :returns: 
    :rtype: 

    """
    try:
        inputdb = 'Ensembl Gene ID'
        outputdb = 'Gene Symbol'

        b = bs.BioDBNet(verbose=True)

        res = b.db2db(input_db=inputdb, output_db=outputdb, input_values=genes, taxon=taxon)

        print("---------------------------")
        print("Show anomalies in mapping: \n", check_mapping_anomalies(df=res, col=outputdb))
        print("---------------------------")

        res.reset_index(inplace=True)

        return res

    except Exception as e:
        print(e)
        while iter < 5:
            get_symb2ensembl(genes, taxon=taxon, iter=iter+1)


def get_id2refseq_mRNA(genes, taxon=9606, iter=0):
    """
    It doesn't matter if it is Gene ID or Symbol to map it to Ensembl.

    :param genes: 
    :param taxon: 
    :param iter: 
    :returns: 
    :rtype: 

    """
    try:
        inputdb = 'Gene ID'
        outputdb = 'RefSeq mRNA Accession'

        b = bs.BioDBNet(verbose=True)

        res = b.db2db(input_db=inputdb, output_db=outputdb, input_values=genes, taxon=taxon)

        print("---------------------------")
        print("Show anomalies in mapping: \n", check_mapping_anomalies(df=res, col=outputdb))
        print("---------------------------")

        res.reset_index(inplace=True)

        return res

    except Exception as e:
        print(e)
        while iter < 5:
            get_symb2ensembl(genes, taxon=taxon, iter=iter+1)


def merge_dfs(dfs, fileout, on='Gene Symbol'):
    """Merge more than 2 dataframes.
    Included import thing of setting the same data type for all merged dataframes.

    :param dfs: list of dataframes
    :param fileout: path where to save merged files
    :param on: column names (list or string), default 'Gene Symbol'
    :returns: merged dataframes
    :rtype: dataframe

    """

    if isinstance(on, list):
        dtype = dict(zip(on, ['str'] * len(on)))

    else:
        dtype = {on, 'str'}

    merged = reduce(lambda left,right: pd.merge(left.astype(dtype),
                                                right.astype(dtype),
                                                on=on,
                                                how='outer'), dfs)

    merged.drop_duplicates(inplace=True)
    merged.reset_index(inplace=True, drop=True)
    merged.to_csv(fileout, index=False)

    return merged


def map_entrez_hgnc(ids, symbol=True, uniprot=True, ensembl=False):

    if isinstance(ids, str):
        ids = [ids]
    else:
        ids = list(ids)

    h = HGNC()

    ## pprint.pprint(h.get_info())

    mapped = []
    for mi in ids:
        row = []
        row.append(mi)
        res = h.fetch('entrez_id', mi)['response']['docs']
        if bool(res) == True:
            res = res[0]
            if uniprot and ('uniprot_ids' in res.keys()):
                row.append('|'.join(res['uniprot_ids']))
            else:
                row.append('-')
            if symbol and ('symbol' in res.keys()):
                row.append(res['symbol'])
            else:
                row.append('-')
            if ensembl and ('ensembl_gene_id' in res.keys()):
                row.append(res['ensembl_gene_id'])
            else:
                row.append('-')

        mapped.append(row)

    dfres = pd.DataFrame(mapped)
    dfres.fillna('-', inplace=True)

    ## dfres = em2.split_single_column(dfres, 'uni', '|')

    return dfres


def map_symbol_hgnc(ids, entrez=True, uniprot=True, ensembl=False):

    if isinstance(ids, str):
        ids = [ids]
    else:
        ids = list(ids)

    h = HGNC()

    ## pprint.pprint(h.get_info())

    mapped = []
    for mi in ids:
        row = []
        row.append(mi)
        res = h.fetch('symbol', mi)['response']['docs']
        if bool(res) == True:
            res = res[0]
            if uniprot and ('uniprot_ids' in res.keys()):
                row.append('|'.join(res['uniprot_ids']))
            else:
                row.append('-')
            if entrez and ('entrez_id' in res.keys()):
                row.append(res['entrez_id'])
            else:
                row.append('-')
            if ensembl and ('ensembl_gene_id' in res.keys()):
                row.append(res['ensembl_gene_id'])
            else:
                row.append('-')

        mapped.append(row)

    dfres = pd.DataFrame(mapped)
    dfres.fillna('-', inplace=True)

    ## dfres = em2.split_single_column(dfres, 'uni', '|')

    return dfres



def map_uniprot_hgnc(ids, symbol=True, entrez=True):

    if isinstance(ids, str):
        ids = [ids]
    else:
        ids = list(ids)

    h = HGNC()

    ## pprint.pprint(h.get_info())

    mapped = []
    for mi in ids:
        row = []
        row.append(mi)
        res = h.fetch('uniprot_ids', mi)['response']['docs']
        if bool(res) == True:
            res = res[0]
            if entrez and ('entrez_id' in res.keys()):
                row.append('|'.join(res['entrez_id']))
            else:
                row.append('-')
            if symbol and ('symbol' in res.keys()):
                row.append(res['symbol'])
            else:
                row.append('-')

        mapped.append(row)

    dfres = pd.DataFrame(mapped)
    dfres.fillna('-', inplace=True)

    return dfres


def map_ensembl_hgnc(ids, symbol=True, uniprot=True, entrez=True):


    if isinstance(ids, str):
        ids = [ids]
    else:
        ids = list(ids)

    h = HGNC()

    ## pprint.pprint(h.get_info())

    mapped = []
    for mi in ids:
        row = []
        row.append(mi)
        res = h.fetch('ensembl_gene_id', mi)['response']['docs']
        if bool(res) == True:
            res = res[0]
            if entrez:
                row.append(res['entrez_id'])
            if uniprot:
                row.append('|'.join(res['uniprot_ids']))
            if symbol:
                row.append(res['symbol'])

        mapped.append(row)

    dfres = pd.DataFrame(mapped, columns=['ens', 'gene', 'uni', 'symb'])
    dfres.fillna('-', inplace=True)

    ## dfres = em2.split_single_column(dfres, 'uni', '|')

    return dfres


def getIndexes(dfObj, value):
    '''
    Get index positions of value in dataframe i.e. dfObj.
    Source: https://thispointer.com/python-find-indexes-of-an-element-in-pandas-dataframe/
    '''
 
    listOfPos = list()
    # Get bool dataframe with True at positions where the given value exists
    result = dfObj.isin([value])
    # Get list of columns that contains the value
    seriesObj = result.any()
    columnNames = list(seriesObj[seriesObj == True].index)
    # Iterate over list of columns and fetch the rows indexes where value exists
    for col in columnNames:
        rows = list(result[col][result[col] == True].index)
        for row in rows:
            listOfPos.append((row, col))
    # Return a list of tuples indicating the positions of value in the dataframe
    return listOfPos


def get_hgnc2symbol(genes, outputpath=False):

    allres = list()

    if isinstance(genes, str):
        genes = [genes]

    h = HGNC()
    for i in genes:
        res = h.fetch('hgnc_id', i)
        if res['response']['docs']:
            res = res['response']['docs'][0]
            allres.append(res)

    ## Turn into dataframe:
    alldf = pd.DataFrame(allres)

    alldfSimp = alldf[['uniprot_ids','symbol', 'hgnc_id', 'ensembl_gene_id', 'entrez_id']]
    alldfSimp['uniprot_ids'] = alldfSimp['uniprot_ids'].apply(lambda x: str(x))
    alldfSimp['uniprot_ids'] = alldfSimp['uniprot_ids'].apply(lambda x: x.strip("[']") if x != 'nan' else '-')
    alldfSimp = em2.split_single_column(alldfSimp, 'uniprot_ids', ', ')

    if outputpath:
        alldf.to_csv(outputpath + '_raw.csv', index=False)
        alldfSimp.to_csv(outputpath + '_selected.csv', index=False)

    return alldf, alldfSimp


def get_symb2all(genes, outputpath=False):
    """Get all possible mappings to gene symbol or gene entrez id.

    :param genes: list of symbols
    :param outputpath: 
    :returns: 
    :rtype: 

    """

    allres = list()

    if isinstance(genes, str):
        genes = [genes]

    h = HGNC()


    if genes[0].isdigit():
        fetchid = "entrez_id"
    else:
        fetchid = "symbol"

    for i in genes:
        res = h.fetch(fetchid, i)
        if res['response']['docs']:
            res = res['response']['docs'][0]
            allres.append(res)

    ## Turn into dataframe:
    alldf = pd.DataFrame(allres)


    alldfSimp = alldf[['uniprot_ids', 'name',
                       'symbol', 'ensembl_gene_id',
                       'entrez_id', 'refseq_accession',
                       'gene_group', 'gene_group_id', 'hgnc_id', 'location', 'locus_type']]


    alldfSimp = alldfSimp.rename(columns = {'uniprot_ids': "Human UniProt Accession",
                                            'name':'Name',
                                            'symbol': 'Human Gene Symbol',
                                            'ensembl_gene_id': 'Human Ensembl Gene ID',
                                            'entrez_id':'Human Gene ID',
                                            'refseq_accession': 'Human Refseq Accession',
                                            'gene_group': 'HGNC Group name', 'gene_group_id': 'HGNC Group ID',
                                            'hgnc_id': 'HGNC ID', 'locus_type': 'HGNC Locus type'})

    # alldfSimp['uniprot_ids'] = alldfSimp['uniprot_ids'].apply(lambda x: str(x))
    # alldfSimp['uniprot_ids'] = alldfSimp['uniprot_ids'].apply(lambda x: x.strip("[']") if x != 'nan' else '-')
    # alldfSimp = em2.split_single_column(alldfSimp, 'uniprot_ids', ', ')

    alldfSimp = alldfSimp.fillna('')
    alldfSimp.loc[:, "Human UniProt Accession"] = alldfSimp.loc[:, "Human UniProt Accession"].apply(lambda x: '|'.join(x))
    alldfSimp.loc[:, 'Human Refseq Accession'] = alldfSimp.loc[:, 'Human Refseq Accession'].apply(lambda x: '|'.join(x))
    alldfSimp.loc[:, 'HGNC Group name'] = alldfSimp.loc[:, 'HGNC Group name'].apply(lambda x: '|'.join(x))
    alldfSimp.loc[:, 'HGNC Group ID'] = alldfSimp.loc[:, 'HGNC Group ID'].apply(lambda x: '|'.join([str(y) for y in x]))

    if outputpath:
        alldf.to_csv(outputpath + '_raw.csv', index=False)
        alldfSimp.to_csv(outputpath + '_selected.csv', index=False)

    return alldf, alldfSimp


def get_symb2synonyms(gene_list, taxon=9606, iter=0):

    try:
        t = bs.BioDBNet()

        input_db = 'Gene Symbol'
        output_db = 'Gene Synonyms'

        res = t.db2db(input_db, output_db, input_values=gene_list, taxon=taxon)

        print("---------------------------")
        print("Show anomalies in mapping: \n", check_mapping_anomalies(df=res, col=output_db))
        print("---------------------------")

        res.reset_index(inplace=True)
        return res

    except Exception as e:
        print(e)
        while iter < 5:
            get_symb2synonyms(gene_list, taxon, iter+1)



def get_synonyms2symb(gene_list, taxon=9606, iter=0):

    try:
        t = bs.BioDBNet()

        output_db = 'Gene Symbol'
        input_db = 'Gene Symbol and Synonyms'

        res = t.db2db(input_db, output_db, input_values=gene_list, taxon=taxon)

        print("---------------------------")
        print("Show anomalies in mapping: \n", check_mapping_anomalies(df=res, col=output_db))
        print("---------------------------")

        res.reset_index(inplace=True)
        return res

    except Exception as e:
        print(e)
        while iter < 5:
            get_synonyms2symb(gene_list, taxon, iter+1)


def get_relevant_ids_from_flat(path, idlist=None, col='u', taxon='9606', header='uniprot'):
    """Get relevant ids to the id list for a given id and taxon.
    Because the list is too big, it is done with the flat file not biodbnet. It would have
    to be somewhere around 1 275 to work or even less.

    :param path: to flat file like bimap_e2u_ncbi_uniprot.csv
    :param idlist: list of ids, either uniprotkb or entrez
    :param col: column to match the idlist on; 'u' for uniprotkb, 'e' for entrez, 's' for symbol
    :param taxon: mouse = 10090 or human = 9606
    :param header: which flat file: uniprot 2 entrez ('uniprot') or entrez 2 symbol ('symbol')
    :returns: dataframe of mapped relevant ids
    :rtype: dataframe

    """

    taxon = str(taxon)

    if header == 'uniprot':
        flat = pd.read_csv(path, dtype=str)
        print(flat.head())

    if header == 'symbol':
        columns = ['t', 'e', 's']
        flat = pd.read_csv(path, dtype=str, names=columns)
        print(flat.head())

    flattax = flat[flat['t'].str.contains(taxon)]
    ## flattax = flattax.drop(columns=['t'])
    flattax.reset_index(inplace=True, drop=True)
    print(flattax.head())

    if idlist is None:
        print('Number of relevant ids: {:d}'.format(len(flattax)) )
        return flattax
    else:
        ## .isin is case sensitive (checked) so it is ok 
        flatmap = flattax[flattax[col].isin(idlist)]
        flatmap.reset_index(inplace=True, drop=True)
        print('Number of relevant ids: {:d}'.format(len(flatmap)) )
        return flatmap


def map_entrez2symbol_flat(df, flatsymbf, taxon):


    idlist = list(set(list(df['geneA'].unique()) + list(df['geneB'].unique())))

    n2e = get_relevant_ids_from_flat(flatsymbf, idlist, col='e', taxon=taxon, header='symbol')

    ## Merge:
    dfs = df.merge(n2e, how="left", left_on="geneA", right_on="e")
    dfs = dfs.drop(columns="e")

    dfs = dfs.merge(n2e, how="left", left_on="geneB", right_on="e")
    dfs = dfs.drop(columns="e")

    ## dfs = dfs.rename(columns={"s_x":"symbA", "s_y":"symbB"})

    return dfs



def replace_missing_entrez_via_symbol(missingID, knownID, taxon, missingIDMapCol, knownIDMapCol,  taxMapCol, mapped):

    if missingID == '' and knownID != '':

        row = mapped[(mapped[knownIDMapCol] == knownID) & (mapped[taxMapCol] == taxon) ].copy()

        if row.empty:
            return missingID

        else:
            return row[missingIDMapCol].item()

    else:
        return missingID


def map_symbol2entrez_flat(df, flatsymbf, taxon):
    """When mapping symbol to entrez it is necessary to get taxon into account in mixed taxon lists.

    DON'T use it because some symbols are depricated and are in fact aliases. It wasn't tested if aliases
    are not mapped to completely different genes. for sure they can be mapped not uniquely.

    :param df: 
    :param flatsymbf: 
    :param taxon: 
    :returns: 
    :rtype: 

    """

    ############### OLD #######################################################################

    ## Case where there is one taxon, it can be used because it wont confuse rat with mouse:

    if '|' not in taxon:

        ## idlist = list(set(list(df['symbA'].unique()) + list(df['symbB'].unique())))
        ## n2e = get_relevant_ids_from_flat(flatsymbf, idlist, col='s', taxon=taxon, header='symbol')

        s2e = get_relevant_ids_from_flat(flatsymbf, col='s', taxon=taxon, header='symbol')

        ## Merge:
        dfs = df.merge(s2e, how="left", left_on="symbA", right_on="s")
        dfs = dfs.drop(columns="s")

        dfs = dfs.merge(s2e, how="left", left_on="symbB", right_on="s")
        dfs = dfs.drop(columns="s")

        dfs = dfs.rename(columns={"e_x":"geneA", "e_y":"geneB"})

        return dfs

    else:
        ########## NEW that replaced columns created to include more than one taxon

        ## Case of multiple taxons:

        ## Map via symbol to entrez:
        e2s = get_relevant_ids_from_flat(flatsymbf, col='s', header='symbol', taxon=taxon)

        geneA = df.apply(lambda x:
                         replace_missing_entrez_via_symbol(x['geneA'], x['symbA'], x['taxA'], 'e', 's', 't', e2s),
                         axis=1)

        geneB = df.apply(lambda x:
                         replace_missing_entrez_via_symbol(x['geneB'], x['symbB'], x['taxB'], 'e', 's', 't', e2s),
                         axis=1)


        df.drop(columns=['geneA', 'geneB'], inplace=True)

        df['geneA'] = geneA
        df['geneB'] = geneB

        ## Drop fatal cases:
        dfF = df[~((df['symbA'] == '') & (df['geneA'] == ''))]
        dfF = dfF[~((dfF['symbB'] == '') & (dfF['geneB'] == ''))]

        return dfF


def map_entrez2uniprot_flat(filepathORdf, mapperfolder, taxon, folderout,
                            chainsort=[ "symbA", "symbB", "entA", "entB", 'geneA', 'geneB'],
                            filerepo=sys.stdout):

    if isinstance(filepathORdf, str):
        df = pd.read_csv(filepathORdf, dtype=str)
    else:
        df = filepathORdf.copy()

    ### Keep ids that are relevant to the ppi dataframe:
    idlist = list(set(list(df['geneA'].unique()) + list(df['geneB'].unique())))

    ### Mapping file:
    mapperfile = mapperfolder + "/bimap_e2u_ncbi_uniprot.csv"

    e2u = get_relevant_ids_from_flat(mapperfile, idlist, 'e', taxon=taxon)

    e2urev = filter_df_with_reviewed_uniprot_acc(e2u, unicol='u', taxon=taxon)

    # notmapped = set(idlist).difference(set(e2urev['e'].unique()))

    # notmapped = pd.DataFrame(notmapped)
    # notmappedfd = folderout.rsplit('/', 1)[0]
    # notmappedF = notmappedfd + "/notmapped.csv"
    # notmapped.to_csv(notmappedF, index=False, header=False)

    ## Merge:
    df = df.merge(e2urev, how="left", left_on="geneA", right_on="e")
    df = df.drop(columns="e")

    df = df.merge(e2urev, how="left", left_on="geneB", right_on="e")
    df = df.drop(columns="e")

    df = df.rename(columns={"u_x":"entA", "u_y":"entB"})

    ## df = map_not_mapped_gene2uniprot_hgnc(df)

    if chainsort:
        em2.chainsort_2_columns_in_df(df, *chainsort)

    df.drop_duplicates(inplace=True)
    df.reset_index(inplace=True, drop=True)
    print("Counts of entries: ", df.count, file=filerepo)

    df.to_csv(folderout,  index=False)

    print("Done", file=filerepo)

    return df



def map_uniprot2entrez_flat(filepathORdf, mapperfolder, taxon, folderout=None,
                            chainsort=[ "symbA", "symbB", "entA", "entB", 'geneA', 'geneB'],
                            filerepo=sys.stdout):

    if isinstance(filepathORdf, str):
        df = pd.read_csv(filepathORdf, dtype=str)
    else:
        df = filepathORdf.copy()

    ### Keep ids that are relevant to the ppi dataframe:
    idlist = list(set(list(df['entA'].unique()) + list(df['entB'].unique())))

    ### Mapping file:
    mapperfile = mapperfolder + "/bimap_e2u_ncbi_uniprot.csv"

    u2e = get_relevant_ids_from_flat(mapperfile, idlist, 'u', taxon=taxon)

    # u2erev = filter_df_with_reviewed_uniprot_acc(u2e, unicol='u', taxon=taxon)

    # notmapped = set(idlist).difference(set(e2urev['e'].unique()))

    # notmapped = pd.DataFrame(notmapped)
    # notmappedfd = folderout.rsplit('/', 1)[0]
    # notmappedF = notmappedfd + "/notmapped.csv"
    # notmapped.to_csv(notmappedF, index=False, header=False)

    ## Merge:
    df = df.merge(u2e, how="left", left_on="entA", right_on="u")
    df = df.drop(columns="u")

    df = df.merge(u2e, how="left", left_on="entB", right_on="u")
    df = df.drop(columns="u")

    df = df.rename(columns={"e_x":"geneA", "e_y":"geneB"})

    df = df.drop(columns=['t_x', 't_y'])

    ## df = map_not_mapped_gene2uniprot_hgnc(df)

    if chainsort:
        em2.chainsort_2_columns_in_df(df, *chainsort)

    df.drop_duplicates(inplace=True)
    df.reset_index(inplace=True, drop=True)
    df.fillna('', inplace=True)

    print("Counts of entries: ", df.count(), file=filerepo)

    if folderout is not None:
        df.to_csv(folderout,  index=False)

    print("Done", file=filerepo)

    return df


def get_geneIDs_species2species(gene_list, intax = '10090', outtax = '9606', iter=0):

    try:
        b = bs.BioDBNet()

        inputdb = 'Gene ID'
        outputdb = 'Gene ID'

        res = b.dbOrtho(input_values=gene_list,
                        input_db=inputdb,
                        input_taxon=intax,
                        output_db=outputdb,
                        output_taxon=outtax)


        # print("---------------------------")
        # print("Show anomalies in mapping: \n", check_mapping_anomalies(df=res, col=outputdb))
        # print("---------------------------")

        # res = res.rename(columns={inputdb: inputdb+str(intax), outputdb: outputdb + str(outtax)})
        # res.reset_index(inplace=True)
        return res

    except Exception as e:
        print(e)
        while iter < 5:
            get_geneIDs_species2species(gene_list, intax, outtax, iter+1)


def replace_wrong_symbols_via_entrez(df, flatsymbf, taxon):

    df.fillna('', inplace=True)

    ## Drop not mapped rows and uniprot accession columns:
    df = df[(df['geneA'] != '') & (df['geneB'] != '')].copy()

    df = map_entrez2symbol_flat(df, flatsymbf, taxon)

    df.fillna('', inplace=True)

    if not df[(df['s_x'] == '') | (df['s_y'] == '')].empty:
        print("Some Entrez ids were not mapped to symbols. ")

        symbA = df.apply(lambda x: x['s_x'] if  x['s_x'] != '' else x['symbA'], axis=1)
        symbB = df.apply(lambda x: x['s_y'] if  x['s_y'] != '' else x['symbB'], axis=1)

        df.drop(columns=['symbA', 'symbB', 't_x', 't_y', 's_x', 's_y'], inplace=True)

        df['symbA'] = symbA
        df['symbB'] = symbB


    else:
        df.drop(columns=['symbA', 'symbB', 't_x', 't_y'], inplace=True)
        df.rename(columns={'s_x': 'symbA', 's_y': 'symbB'}, inplace=True)


    df = em2.chainsort_2_columns_in_df(df, 'geneA', 'geneB', 'symbA', 'symbB', 'taxA', 'taxB')

    df.drop_duplicates(inplace=True)
    df.reset_index(inplace=True, drop=True)

    return df



def map_mouse_rat_to_human_entrez_ids_flat(orthoflat, df, colA='entA', colB='entB' ):

    ortho = pd.read_csv(orthoflat, sep='\t')
    ortho = em2.chainsort_2_columns_in_df(ortho, '#tax_id', 'Other_tax_id', 'GeneID', 'Other_GeneID' )

    mouseRatHuman = ortho[ortho['#tax_id'].isin([9606]) & ortho['Other_tax_id'].isin([10090, 10116])].copy()
    mouseRatHuman.reset_index(inplace=True, drop=True)

    ## Change data frame:
    df.loc[:, colA] = df[colA].astype(int)
    df.loc[:, colB] = df[colB].astype(int)

    df.loc[:, 'taxA'] = df['taxA'].astype(int)
    df.loc[:, 'taxB'] = df['taxB'].astype(int)


    otherhuman = df[(df['taxA'] != 9606) | (df['taxB'] != 9606)]

    entA = otherhuman.apply(lambda x: x[colA] if x['taxA'] !=  9606 else '-' , axis=1)
    entB = otherhuman.apply(lambda x: x[colB] if x['taxB'] !=  9606 else '-' , axis=1)

    toMap = list(set(list(entB.unique()) + list(entA.unique())))
    toMap = [int(i) for i in toMap if i != '-']

    mappedMRH = mouseRatHuman[mouseRatHuman['Other_GeneID'].isin(toMap)].copy()
    notmaped = set(toMap).difference(set(mappedMRH['Other_GeneID'].to_list()))

    print("There are "+ str(len(notmaped)) + "Mouse/Rat genes that do not have human orthologs.")

    mappedDict = em2.df2dict(mappedMRH[['Other_GeneID', 'GeneID']])


    def insert_mapping(ent, tax):

        if ent in mappedDict.keys():
            return mappedDict[ent]

        elif tax == 9606:
            return ent

        else:
            return ''

    df.loc[:, colA + '_Human'] = df.apply(lambda x: insert_mapping(x[colA], x['taxA']), axis=1)
    df.loc[:, colB + '_Human'] = df.apply(lambda x: insert_mapping(x[colB], x['taxB']), axis=1)

    return df
