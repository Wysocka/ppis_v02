import os
import matplotlib
import math
from functools import reduce
import pandas as pd
import numpy as np


def is_prime(n):
    if n % 2 == 0 and n > 2:
        return False
    for i in range(3, int(math.sqrt(n)) + 1, 2):
        if n % i == 0:
            return False
    return True


def factorise_2(integer, i=9):
    if is_prime(integer) and integer > 3:
        integer += 1

    if (integer%i == 0) and (i!=0):
        return integer/i, i
    else:
        return factorise_2(integer, i-1)


def _divide_to_chunks(lista, no):
    """
    Divide list into chunks of size no.
    :param lista: input list to be divided
    :param no: size of the chunk in int
    :return:
    """
    output = [lista[i:i+no] for i in range(0, len(lista), no)]
    return output


def timeseries_2_array(tsfile, delim = ' '):
    """
    Transform raw KaSim output simulation into numpy array

    :param tsfile:
    :param delim:
    :return: column labels as list and array of data
   """
    try:
        tsdata = open(tsfile, 'r').readlines()
        # cleaning from artefacts in Kappa .out file:
        tsdataC = [i.strip('\n').lstrip('# ') for i in tsdata]

        # remove column with time:
        tsdata2 = [i.split(delim)[1:] for i in tsdataC]

        try:
            # remove empty string at the end from each time point and don't take empty lists
            tsdata3 = [filter(None, i) for i in tsdata2 if i != []]
            labels = [i.strip("''") for i in tsdata3[0]]
            # check if the number of elements in each list is right:
            ncols = len(labels)
            for col in tsdata3:
                assert len(col) == ncols,"Not equal number of columns. Missing value ? in columns: "+str(col)
            # turn strings to float:
            tsdata4 = [map(float,i)  for i in tsdata3[1:]]
            return labels, np.array(tsdata4, dtype=float)
        except (ValueError) as e:
            print("Error "+ e + " with case " + str(i))
            return 0
    except IOError:
        print("Error: file doesn't exist.")


def filter_mirrored_duplicates(dataFrame, col1, col2):
    """ Remove mirrored duplicates in dataframe in two columns

    :param dataFrame: dataframe
    :param col1: column name
    :param col2: column name
    :returns: copy of dataframe with removed duplicates
    :rtype: dataframe

    """

    df = dataFrame[dataFrame[col1] > dataFrame[col2]]
    to_drop = df.select(lambda x: ((dataFrame[col2] == df[col1].loc[x]) & (dataFrame[col1] == df[col2].loc[x])).any())

    dataFrameCopy = dataFrame.copy()
    return dataFrameCopy.drop(to_drop.index)

def invert_dict(dict_of_lists):
    """Now keys are values and values are keys where values can be lists.

    :param dict_of_lists: 
    :returns: 
    :rtype: 

    """
    dict_of_lists_inv = {}
    for key, val in dict_of_lists.iteritems():
        for item in val:
            dict_of_lists_inv.setdefault(item, []).append(key)

    return dict_of_lists_inv


def do_humansorting(lista):

    import re

    def tryint(s):
        try:
            return int(s)
        except:
            return s
    def alphanum_key(s):
        """ Turn a string into a list of string and number chunks.
            "z23a" -> ["z", 23, "a"]
        """
        return [ tryint(c) for c in re.split('([0-9]+)', s) ]

    def sort_nicely(l):
        """ Sort the given list in the way that humans expect.
        """
        l.sort(key=alphanum_key)

    return sort_nicely(lista)


def df2dict(df):

    assert len(df.iloc[:, 0].unique()) >= len(df.iloc[:, 1].unique()), "More keys than values.  "

    return {df.iloc[i][0]: df.iloc[i][1] for i in range(len(df))}



def split_single_column(df, target_column, separator):
        '''
        TO BE USED WITH THESE FILES WHICH NEED MAPPING UNIPROT ACC TO ENTREZ

        :param df:  dataframe to be split
        :param target_column: the column containing the values to be split
        :param separator: the symbol used to perform the split

        returns: a dataframe with each entry for the target column separated, with each element moved into a new row.
        The values in the other columns are duplicated across the newly divided rows.
        At the end the method removes duplicated rows
        '''

        def splitListToRows(row, row_accumulator, target_column, separator):
            split_row = row[target_column].split(separator)
            for s in split_row:
                new_row = row.to_dict()
                new_row[target_column] = s
                row_accumulator.append(new_row)
        new_rows = []
        df.apply(splitListToRows, axis=1, args=(new_rows, target_column, separator))
        new_df = pd.DataFrame(new_rows)
        return new_df.drop_duplicates()


def fill_df_with_dict(df, dic, keyCol='Mouse Gene ID', valCol='Mouse UniProt Accession'):
    """Replaces cells in a dataframe in one column with values in dict when the dict's key matches
    value in another column.

    :param df: dataframe
    :param dic: dictionary with keys matching values in one column and values that replace values in another column.
    :param keyCol: name of a column of the dict key
    :param valCol: name of a column of the dict values
    :returns: copy of dataframe with filled in values from dict
    :rtype: dataframe

    """
    dfcopy = df.copy()

    for i in range(len(dic)):
        key = list(dic.keys())[i]
        dfcopy.loc[dfcopy[keyCol] == key, valCol] = dic[key]

    return dfcopy


def get_color_name2hex():

    name2hex = []
    for name, hex in matplotlib.colors.cnames.items():
        name2hex.append([name, hex])

    return name2hex


def sort_2_columns_in_df(df, col1, col2):
    df.loc[:,(col1,col2)] = df[[col1,col2]].where(df[col1] < df[col2], df[[col2,col1]].values)

    return df


def chainsort_2_columns_in_df(df, col1A, col1B, col2A=False, col2B=False, col3A=False, col3B=False):

    if col3A and col3B:
        df.loc[:, [col1A, col1B, col2A, col2B, col3A, col3B]] = df.loc[:, [col1A, col1B, col2A, col2B, col3A, col3B]].where(df[col1A] < df[col1B], df[[col1B,col1A, col2B,col2A, col3B, col3A]].values)

    elif col2A and col2B:
        df.loc[:,[col1A, col1B, col2A, col2B]] = df.loc[:, [col1A, col1B, col2A, col2B]].where(df[col1A] < df[col1B], df[[col1B,col1A,col2B,col2A]].values)

    else:
        df.loc[:, [col1A, col1B]] = df.loc[:, [col1A, col1B]].where(df[col1A] < df[col1B], df[[col1B,col1A]].values)

    return df



def print_column_types(df):

    for i in range(len(df.columns)):
        print(df.columns[i], type(df.iloc[1,i]))


def merge_dfs(dfs, fileout=None, on='Gene Symbol'):
    """Merge more than 2 dataframes.
    Included import thing of setting the same data type for all merged dataframes.

    :param dfs: list of dataframes
    :param fileout: path where to save merged files
    :param on: column names (list or string), default 'Gene Symbol'
    :returns: merged dataframes
    :rtype: dataframe

    """

    if isinstance(on, list):
        dtype = dict(zip(on, ['str'] * len(on)))

    else:
        dtype = {on, 'str'}

    merged = reduce(lambda left,right: pd.merge(left.astype(dtype),
                                                right.astype(dtype),
                                                on=on,
                                                how='outer'), dfs)

    merged.drop_duplicates(inplace=True)
    merged.reset_index(inplace=True, drop=True)

    if fileout is not None:
        merged.to_csv(fileout, index=False)

    return merged


def get_df1_df2_difference(biggerdf, smallerdf):

    res = biggerdf.merge(smallerdf, indicator=True, how='left').loc[lambda x: x['_merge'] != 'both']
    ## res.drop(columns='_merge', inplace=True)

    return res


def get_df1_df2_common(biggerdf, smallerdf):

    if '_merge' in biggerdf:
        biggerdf.drop(axis=1, columns='_merge', inplace=True)
    if '_merge' in smallerdf:
        smallerdf.drop(axis=1, columns='_merge', inplace=True)

    return biggerdf.merge(smallerdf, indicator=True, how='left').loc[lambda x: x['_merge'] == 'both']


def print_full():
    pd.set_option('display.max_columns', None)  # or 1000
    pd.set_option('display.max_rows', None)  # or 1000
    pd.set_option('display.max_colwidth', -1)  # or 199


def make_folder(newpath):
    if not os.path.exists(newpath):
        os.makedirs(newpath)


def get_entities_of_2_cols(df, col1, col2):

    assert isinstance(col1, str) and isinstance(col2, str), "Columns must be strings." 
    res = list(set(list(df[col1].unique() ) + list(df[col2].unique())))

    ## Filter out different types of missing values (de Morgan's):
    return [i for i in res if i != '' and i != '-' and i is not np.nan]


def is_integer(value: str, *, base: int=10) -> bool:
    try:
        int(value, base=base)
        return True
    except ValueError:
        return False

def insert_column_in_front(df, col ):
    symcol = df[col]
    df = df.drop(columns=[col])
    df.insert(0, column= col, value = symcol)

    return df


def try_extract(pattern, string):
    try:
        m = pattern.search(string)
        return m.group(0)
    except (TypeError, ValueError, AttributeError):
        return string


def try_extract_empty(pattern, string):
    try:
        m = pattern.search(string)
        return m.group(0)
    except (TypeError, ValueError, AttributeError):
        return ''

