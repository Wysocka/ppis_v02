#!/usr/bin/env python

import pandas as pd
import os
import toolbox.get_mi_terms as gmt
import toolbox.id_mapping_tools as imt
import toolbox.emtools2 as em2
import toolbox.ppi_pars_tools as ppt
import subprocess
from toolbox import houseKeeper

"""
 ----------- Preparation of PPIs files ----------------- 

"""

def main():

    ###############################
    ##   Get mapping files
    ###############################
    # subprocess.check_call('python3.7 ./make_mappingFiles.py', shell=True)

    ###############################
    ##   Inputs
    ###############################
    taxon = "9606|10090|10116"
    taxon_name = taxon.replace('|', '_')

    hk = houseKeeper.HouseKeeper()

    parentfold = os.path.abspath('..')
    currentfold = os.path.abspath('.')
    

    excludedmethods = "./mi_terms/excluded.txt"
    flatsymbf = hk.get_mapping_files_path() + "/gene2accession.9606_10090_10116_ent2symb_ncbi.cleaned.csv"
    orthoF = hk.get_mapping_files_path() + "/gene_orthologs"

    folderout = parentfold + "/"+hk.datetoday+"-PPI-human-final_shared"

    if not os.path.exists(folderout):
        os.mkdir(folderout)

    ###############################
    ## MI ontology files:
    ###############################
    owlfolder=currentfold + "/mi_terms"
    mionto = gmt.MIfile(owlfolder)

    ############################
    ##   IntAct
    ############################
    intfile, biogridfile = ppt.dowload_biogrid_intact()
    inta, intapath = ppt.pars_intact_flat(intfile,  taxon=taxon)

    ## Remove interactions from spoke:
    inta = inta.loc[inta['spoke'] == '-', :]
    inta.drop(columns=['spoke'], inplace=True)

    ## MAPPING uniprot to entrez genes:
    intaM = imt.map_uniprot2entrez_flat(inta, mapperfolder = hk.cleaned_mappers, taxon=taxon)
    intaMDM = imt.replace_wrong_symbols_via_entrez(intaM, flatsymbf, taxon)
    intaMDM.drop(columns=['entA','entB'], inplace=True)

    ############################
    ##   BioGRID
    ############################

    bio, biofile = ppt.pars_biogrid_flat(biogridfile, taxon=taxon)
    bioMDM = imt.replace_wrong_symbols_via_entrez(bio, flatsymbf, taxon)

    ############################
    ##   Merge
    ############################

    all_Midb = pd.concat([intaMDM, bioMDM], ignore_index=True, sort=True)
    all_Midb.rename(columns={'geneA': 'entA', 'geneB':'entB'}, inplace=True)
    all_Midb.drop(columns='sourcedb', inplace=True)

    all_Midbsorted = em2.chainsort_2_columns_in_df(all_Midb, "entA", "entB", 'symbA', 'symbB', 'taxA', 'taxB')
    all_Midbsorted.drop_duplicates(inplace=True)
    all_Midbsorted.reset_index(drop=True, inplace=True)

    ### Save as csv file:
    csvf = "/SimpsonArmstrongPPI_"+ hk.datetoday +".csv"

    all_Midbsorted.to_csv(folderout + csvf, index = False, sep = '\t')

    ## Load direct interactions ids:
    associations = mionto.get_association_terms()

    associations["MI:0218"] = "obsolete: physical interaction"

    ## Save to file:
    associations_tup = [(key,val) for key, val in associations.items()]
    associations_df = pd.DataFrame(associations_tup)

    associations_df.to_csv("./mi_terms/MI_association_terms_" + hk.datetoday + ".tab", sep='\t', header=None, index=False)

    ## Leave only interactions in the bag of association + association:
    ppi_assoc = all_Midbsorted[all_Midbsorted['type'].isin(associations.keys())]

    ### Save to csv file:
    csvf_dir = "/PPI_" + taxon_name+ "_associations_final.csv"

    ppi_assoc.to_csv(folderout + csvf_dir, sep='\t', index = False)


    ###########################################
    ## Filter with excluded from Ian:
    ###########################################
    excluded = pd.read_csv(excludedmethods, header=None)[0].to_list()

    ## Leave only interactions in the bag of association + association:
    ppi_assoc_excluded = ppi_assoc[~ppi_assoc['method'].isin(excluded)]

    ## Save:
    ppi_assoc_excluded.to_csv(folderout + "/PPI_" + taxon_name+ "_associations_excluded_final.csv")

    ########################
    ## MAP TO HUMAN:
    ########################
    ppi_assoc_excludedM = imt.map_mouse_rat_to_human_entrez_ids_flat(orthoflat=orthoF, df=ppi_assoc_excluded)

    ## Save:
    ppi_assoc_excludedM.to_csv(folderout + "/PPI_" + taxon_name+ "_associations_excluded_final_HumanMapped.csv", index=False)


if __name__ == "__main__":
    main()